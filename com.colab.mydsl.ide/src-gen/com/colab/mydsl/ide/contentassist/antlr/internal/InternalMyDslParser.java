package com.colab.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import com.colab.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_STRING", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'dailymotion'", "'deezer'", "'LIST'", "'MOZAIC'", "'site'", "'apis'", "'tags'", "','", "'weather'", "'settings :'", "'company'", "'town'", "'country'", "'itemVideo'", "'number'", "'colorPicker'", "'default'", "'displayItem'", "'searchBar'", "'sortBar'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int RULE_ID=6;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleSite"
    // InternalMyDsl.g:53:1: entryRuleSite : ruleSite EOF ;
    public final void entryRuleSite() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleSite EOF )
            // InternalMyDsl.g:55:1: ruleSite EOF
            {
             before(grammarAccess.getSiteRule()); 
            pushFollow(FOLLOW_1);
            ruleSite();

            state._fsp--;

             after(grammarAccess.getSiteRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSite"


    // $ANTLR start "ruleSite"
    // InternalMyDsl.g:62:1: ruleSite : ( ( rule__Site__Group__0 ) ) ;
    public final void ruleSite() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Site__Group__0 ) ) )
            // InternalMyDsl.g:67:2: ( ( rule__Site__Group__0 ) )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Site__Group__0 ) )
            // InternalMyDsl.g:68:3: ( rule__Site__Group__0 )
            {
             before(grammarAccess.getSiteAccess().getGroup()); 
            // InternalMyDsl.g:69:3: ( rule__Site__Group__0 )
            // InternalMyDsl.g:69:4: rule__Site__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Site__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSiteAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSite"


    // $ANTLR start "entryRuleCompany"
    // InternalMyDsl.g:78:1: entryRuleCompany : ruleCompany EOF ;
    public final void entryRuleCompany() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleCompany EOF )
            // InternalMyDsl.g:80:1: ruleCompany EOF
            {
             before(grammarAccess.getCompanyRule()); 
            pushFollow(FOLLOW_1);
            ruleCompany();

            state._fsp--;

             after(grammarAccess.getCompanyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCompany"


    // $ANTLR start "ruleCompany"
    // InternalMyDsl.g:87:1: ruleCompany : ( ( rule__Company__Group__0 ) ) ;
    public final void ruleCompany() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Company__Group__0 ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Company__Group__0 ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Company__Group__0 ) )
            // InternalMyDsl.g:93:3: ( rule__Company__Group__0 )
            {
             before(grammarAccess.getCompanyAccess().getGroup()); 
            // InternalMyDsl.g:94:3: ( rule__Company__Group__0 )
            // InternalMyDsl.g:94:4: rule__Company__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Company__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCompanyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompany"


    // $ANTLR start "entryRuleTag"
    // InternalMyDsl.g:103:1: entryRuleTag : ruleTag EOF ;
    public final void entryRuleTag() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleTag EOF )
            // InternalMyDsl.g:105:1: ruleTag EOF
            {
             before(grammarAccess.getTagRule()); 
            pushFollow(FOLLOW_1);
            ruleTag();

            state._fsp--;

             after(grammarAccess.getTagRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTag"


    // $ANTLR start "ruleTag"
    // InternalMyDsl.g:112:1: ruleTag : ( ( rule__Tag__Group__0 ) ) ;
    public final void ruleTag() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__Tag__Group__0 ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__Tag__Group__0 ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__Tag__Group__0 ) )
            // InternalMyDsl.g:118:3: ( rule__Tag__Group__0 )
            {
             before(grammarAccess.getTagAccess().getGroup()); 
            // InternalMyDsl.g:119:3: ( rule__Tag__Group__0 )
            // InternalMyDsl.g:119:4: rule__Tag__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Tag__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTagAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTag"


    // $ANTLR start "entryRuleApi"
    // InternalMyDsl.g:128:1: entryRuleApi : ruleApi EOF ;
    public final void entryRuleApi() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleApi EOF )
            // InternalMyDsl.g:130:1: ruleApi EOF
            {
             before(grammarAccess.getApiRule()); 
            pushFollow(FOLLOW_1);
            ruleApi();

            state._fsp--;

             after(grammarAccess.getApiRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleApi"


    // $ANTLR start "ruleApi"
    // InternalMyDsl.g:137:1: ruleApi : ( ( rule__Api__Group__0 ) ) ;
    public final void ruleApi() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__Api__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__Api__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__Api__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__Api__Group__0 )
            {
             before(grammarAccess.getApiAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__Api__Group__0 )
            // InternalMyDsl.g:144:4: rule__Api__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Api__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getApiAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleApi"


    // $ANTLR start "entryRuleWeatherFunct"
    // InternalMyDsl.g:153:1: entryRuleWeatherFunct : ruleWeatherFunct EOF ;
    public final void entryRuleWeatherFunct() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleWeatherFunct EOF )
            // InternalMyDsl.g:155:1: ruleWeatherFunct EOF
            {
             before(grammarAccess.getWeatherFunctRule()); 
            pushFollow(FOLLOW_1);
            ruleWeatherFunct();

            state._fsp--;

             after(grammarAccess.getWeatherFunctRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWeatherFunct"


    // $ANTLR start "ruleWeatherFunct"
    // InternalMyDsl.g:162:1: ruleWeatherFunct : ( ( rule__WeatherFunct__Group__0 ) ) ;
    public final void ruleWeatherFunct() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__WeatherFunct__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__WeatherFunct__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__WeatherFunct__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__WeatherFunct__Group__0 )
            {
             before(grammarAccess.getWeatherFunctAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__WeatherFunct__Group__0 )
            // InternalMyDsl.g:169:4: rule__WeatherFunct__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__WeatherFunct__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWeatherFunctAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWeatherFunct"


    // $ANTLR start "entryRuleItemNumberFunct"
    // InternalMyDsl.g:178:1: entryRuleItemNumberFunct : ruleItemNumberFunct EOF ;
    public final void entryRuleItemNumberFunct() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( ruleItemNumberFunct EOF )
            // InternalMyDsl.g:180:1: ruleItemNumberFunct EOF
            {
             before(grammarAccess.getItemNumberFunctRule()); 
            pushFollow(FOLLOW_1);
            ruleItemNumberFunct();

            state._fsp--;

             after(grammarAccess.getItemNumberFunctRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleItemNumberFunct"


    // $ANTLR start "ruleItemNumberFunct"
    // InternalMyDsl.g:187:1: ruleItemNumberFunct : ( ( rule__ItemNumberFunct__Group__0 ) ) ;
    public final void ruleItemNumberFunct() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__ItemNumberFunct__Group__0 ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__ItemNumberFunct__Group__0 ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__ItemNumberFunct__Group__0 ) )
            // InternalMyDsl.g:193:3: ( rule__ItemNumberFunct__Group__0 )
            {
             before(grammarAccess.getItemNumberFunctAccess().getGroup()); 
            // InternalMyDsl.g:194:3: ( rule__ItemNumberFunct__Group__0 )
            // InternalMyDsl.g:194:4: rule__ItemNumberFunct__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ItemNumberFunct__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getItemNumberFunctAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleItemNumberFunct"


    // $ANTLR start "entryRuleColorPickerFunct"
    // InternalMyDsl.g:203:1: entryRuleColorPickerFunct : ruleColorPickerFunct EOF ;
    public final void entryRuleColorPickerFunct() throws RecognitionException {
        try {
            // InternalMyDsl.g:204:1: ( ruleColorPickerFunct EOF )
            // InternalMyDsl.g:205:1: ruleColorPickerFunct EOF
            {
             before(grammarAccess.getColorPickerFunctRule()); 
            pushFollow(FOLLOW_1);
            ruleColorPickerFunct();

            state._fsp--;

             after(grammarAccess.getColorPickerFunctRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleColorPickerFunct"


    // $ANTLR start "ruleColorPickerFunct"
    // InternalMyDsl.g:212:1: ruleColorPickerFunct : ( ( rule__ColorPickerFunct__Group__0 ) ) ;
    public final void ruleColorPickerFunct() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:216:2: ( ( ( rule__ColorPickerFunct__Group__0 ) ) )
            // InternalMyDsl.g:217:2: ( ( rule__ColorPickerFunct__Group__0 ) )
            {
            // InternalMyDsl.g:217:2: ( ( rule__ColorPickerFunct__Group__0 ) )
            // InternalMyDsl.g:218:3: ( rule__ColorPickerFunct__Group__0 )
            {
             before(grammarAccess.getColorPickerFunctAccess().getGroup()); 
            // InternalMyDsl.g:219:3: ( rule__ColorPickerFunct__Group__0 )
            // InternalMyDsl.g:219:4: rule__ColorPickerFunct__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ColorPickerFunct__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getColorPickerFunctAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleColorPickerFunct"


    // $ANTLR start "entryRuleDisplayItemFunct"
    // InternalMyDsl.g:228:1: entryRuleDisplayItemFunct : ruleDisplayItemFunct EOF ;
    public final void entryRuleDisplayItemFunct() throws RecognitionException {
        try {
            // InternalMyDsl.g:229:1: ( ruleDisplayItemFunct EOF )
            // InternalMyDsl.g:230:1: ruleDisplayItemFunct EOF
            {
             before(grammarAccess.getDisplayItemFunctRule()); 
            pushFollow(FOLLOW_1);
            ruleDisplayItemFunct();

            state._fsp--;

             after(grammarAccess.getDisplayItemFunctRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDisplayItemFunct"


    // $ANTLR start "ruleDisplayItemFunct"
    // InternalMyDsl.g:237:1: ruleDisplayItemFunct : ( ( rule__DisplayItemFunct__Group__0 ) ) ;
    public final void ruleDisplayItemFunct() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:2: ( ( ( rule__DisplayItemFunct__Group__0 ) ) )
            // InternalMyDsl.g:242:2: ( ( rule__DisplayItemFunct__Group__0 ) )
            {
            // InternalMyDsl.g:242:2: ( ( rule__DisplayItemFunct__Group__0 ) )
            // InternalMyDsl.g:243:3: ( rule__DisplayItemFunct__Group__0 )
            {
             before(grammarAccess.getDisplayItemFunctAccess().getGroup()); 
            // InternalMyDsl.g:244:3: ( rule__DisplayItemFunct__Group__0 )
            // InternalMyDsl.g:244:4: rule__DisplayItemFunct__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DisplayItemFunct__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDisplayItemFunctAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDisplayItemFunct"


    // $ANTLR start "entryRuleSearchBarFunct"
    // InternalMyDsl.g:253:1: entryRuleSearchBarFunct : ruleSearchBarFunct EOF ;
    public final void entryRuleSearchBarFunct() throws RecognitionException {
        try {
            // InternalMyDsl.g:254:1: ( ruleSearchBarFunct EOF )
            // InternalMyDsl.g:255:1: ruleSearchBarFunct EOF
            {
             before(grammarAccess.getSearchBarFunctRule()); 
            pushFollow(FOLLOW_1);
            ruleSearchBarFunct();

            state._fsp--;

             after(grammarAccess.getSearchBarFunctRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSearchBarFunct"


    // $ANTLR start "ruleSearchBarFunct"
    // InternalMyDsl.g:262:1: ruleSearchBarFunct : ( ( rule__SearchBarFunct__Group__0 ) ) ;
    public final void ruleSearchBarFunct() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:266:2: ( ( ( rule__SearchBarFunct__Group__0 ) ) )
            // InternalMyDsl.g:267:2: ( ( rule__SearchBarFunct__Group__0 ) )
            {
            // InternalMyDsl.g:267:2: ( ( rule__SearchBarFunct__Group__0 ) )
            // InternalMyDsl.g:268:3: ( rule__SearchBarFunct__Group__0 )
            {
             before(grammarAccess.getSearchBarFunctAccess().getGroup()); 
            // InternalMyDsl.g:269:3: ( rule__SearchBarFunct__Group__0 )
            // InternalMyDsl.g:269:4: rule__SearchBarFunct__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SearchBarFunct__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSearchBarFunctAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSearchBarFunct"


    // $ANTLR start "entryRuleSortBarFunct"
    // InternalMyDsl.g:278:1: entryRuleSortBarFunct : ruleSortBarFunct EOF ;
    public final void entryRuleSortBarFunct() throws RecognitionException {
        try {
            // InternalMyDsl.g:279:1: ( ruleSortBarFunct EOF )
            // InternalMyDsl.g:280:1: ruleSortBarFunct EOF
            {
             before(grammarAccess.getSortBarFunctRule()); 
            pushFollow(FOLLOW_1);
            ruleSortBarFunct();

            state._fsp--;

             after(grammarAccess.getSortBarFunctRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSortBarFunct"


    // $ANTLR start "ruleSortBarFunct"
    // InternalMyDsl.g:287:1: ruleSortBarFunct : ( ( rule__SortBarFunct__Group__0 ) ) ;
    public final void ruleSortBarFunct() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:291:2: ( ( ( rule__SortBarFunct__Group__0 ) ) )
            // InternalMyDsl.g:292:2: ( ( rule__SortBarFunct__Group__0 ) )
            {
            // InternalMyDsl.g:292:2: ( ( rule__SortBarFunct__Group__0 ) )
            // InternalMyDsl.g:293:3: ( rule__SortBarFunct__Group__0 )
            {
             before(grammarAccess.getSortBarFunctAccess().getGroup()); 
            // InternalMyDsl.g:294:3: ( rule__SortBarFunct__Group__0 )
            // InternalMyDsl.g:294:4: rule__SortBarFunct__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SortBarFunct__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSortBarFunctAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSortBarFunct"


    // $ANTLR start "entryRuleEInt"
    // InternalMyDsl.g:303:1: entryRuleEInt : ruleEInt EOF ;
    public final void entryRuleEInt() throws RecognitionException {
        try {
            // InternalMyDsl.g:304:1: ( ruleEInt EOF )
            // InternalMyDsl.g:305:1: ruleEInt EOF
            {
             before(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getEIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalMyDsl.g:312:1: ruleEInt : ( RULE_INT ) ;
    public final void ruleEInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:316:2: ( ( RULE_INT ) )
            // InternalMyDsl.g:317:2: ( RULE_INT )
            {
            // InternalMyDsl.g:317:2: ( RULE_INT )
            // InternalMyDsl.g:318:3: RULE_INT
            {
             before(grammarAccess.getEIntAccess().getINTTerminalRuleCall()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEIntAccess().getINTTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleEString"
    // InternalMyDsl.g:328:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalMyDsl.g:329:1: ( ruleEString EOF )
            // InternalMyDsl.g:330:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalMyDsl.g:337:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:341:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalMyDsl.g:342:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalMyDsl.g:342:2: ( ( rule__EString__Alternatives ) )
            // InternalMyDsl.g:343:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalMyDsl.g:344:3: ( rule__EString__Alternatives )
            // InternalMyDsl.g:344:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "ruleAPIEnum"
    // InternalMyDsl.g:353:1: ruleAPIEnum : ( ( rule__APIEnum__Alternatives ) ) ;
    public final void ruleAPIEnum() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:357:1: ( ( ( rule__APIEnum__Alternatives ) ) )
            // InternalMyDsl.g:358:2: ( ( rule__APIEnum__Alternatives ) )
            {
            // InternalMyDsl.g:358:2: ( ( rule__APIEnum__Alternatives ) )
            // InternalMyDsl.g:359:3: ( rule__APIEnum__Alternatives )
            {
             before(grammarAccess.getAPIEnumAccess().getAlternatives()); 
            // InternalMyDsl.g:360:3: ( rule__APIEnum__Alternatives )
            // InternalMyDsl.g:360:4: rule__APIEnum__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__APIEnum__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAPIEnumAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAPIEnum"


    // $ANTLR start "ruleDisplayItemChoiceEnum"
    // InternalMyDsl.g:369:1: ruleDisplayItemChoiceEnum : ( ( rule__DisplayItemChoiceEnum__Alternatives ) ) ;
    public final void ruleDisplayItemChoiceEnum() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:373:1: ( ( ( rule__DisplayItemChoiceEnum__Alternatives ) ) )
            // InternalMyDsl.g:374:2: ( ( rule__DisplayItemChoiceEnum__Alternatives ) )
            {
            // InternalMyDsl.g:374:2: ( ( rule__DisplayItemChoiceEnum__Alternatives ) )
            // InternalMyDsl.g:375:3: ( rule__DisplayItemChoiceEnum__Alternatives )
            {
             before(grammarAccess.getDisplayItemChoiceEnumAccess().getAlternatives()); 
            // InternalMyDsl.g:376:3: ( rule__DisplayItemChoiceEnum__Alternatives )
            // InternalMyDsl.g:376:4: rule__DisplayItemChoiceEnum__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DisplayItemChoiceEnum__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDisplayItemChoiceEnumAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDisplayItemChoiceEnum"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalMyDsl.g:384:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:388:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_STRING) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalMyDsl.g:389:2: ( RULE_STRING )
                    {
                    // InternalMyDsl.g:389:2: ( RULE_STRING )
                    // InternalMyDsl.g:390:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:395:2: ( RULE_ID )
                    {
                    // InternalMyDsl.g:395:2: ( RULE_ID )
                    // InternalMyDsl.g:396:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__APIEnum__Alternatives"
    // InternalMyDsl.g:405:1: rule__APIEnum__Alternatives : ( ( ( 'dailymotion' ) ) | ( ( 'deezer' ) ) );
    public final void rule__APIEnum__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:409:1: ( ( ( 'dailymotion' ) ) | ( ( 'deezer' ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:410:2: ( ( 'dailymotion' ) )
                    {
                    // InternalMyDsl.g:410:2: ( ( 'dailymotion' ) )
                    // InternalMyDsl.g:411:3: ( 'dailymotion' )
                    {
                     before(grammarAccess.getAPIEnumAccess().getDailymotionEnumLiteralDeclaration_0()); 
                    // InternalMyDsl.g:412:3: ( 'dailymotion' )
                    // InternalMyDsl.g:412:4: 'dailymotion'
                    {
                    match(input,11,FOLLOW_2); 

                    }

                     after(grammarAccess.getAPIEnumAccess().getDailymotionEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:416:2: ( ( 'deezer' ) )
                    {
                    // InternalMyDsl.g:416:2: ( ( 'deezer' ) )
                    // InternalMyDsl.g:417:3: ( 'deezer' )
                    {
                     before(grammarAccess.getAPIEnumAccess().getDeezerEnumLiteralDeclaration_1()); 
                    // InternalMyDsl.g:418:3: ( 'deezer' )
                    // InternalMyDsl.g:418:4: 'deezer'
                    {
                    match(input,12,FOLLOW_2); 

                    }

                     after(grammarAccess.getAPIEnumAccess().getDeezerEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__APIEnum__Alternatives"


    // $ANTLR start "rule__DisplayItemChoiceEnum__Alternatives"
    // InternalMyDsl.g:426:1: rule__DisplayItemChoiceEnum__Alternatives : ( ( ( 'LIST' ) ) | ( ( 'MOZAIC' ) ) );
    public final void rule__DisplayItemChoiceEnum__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:430:1: ( ( ( 'LIST' ) ) | ( ( 'MOZAIC' ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:431:2: ( ( 'LIST' ) )
                    {
                    // InternalMyDsl.g:431:2: ( ( 'LIST' ) )
                    // InternalMyDsl.g:432:3: ( 'LIST' )
                    {
                     before(grammarAccess.getDisplayItemChoiceEnumAccess().getLISTEnumLiteralDeclaration_0()); 
                    // InternalMyDsl.g:433:3: ( 'LIST' )
                    // InternalMyDsl.g:433:4: 'LIST'
                    {
                    match(input,13,FOLLOW_2); 

                    }

                     after(grammarAccess.getDisplayItemChoiceEnumAccess().getLISTEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:437:2: ( ( 'MOZAIC' ) )
                    {
                    // InternalMyDsl.g:437:2: ( ( 'MOZAIC' ) )
                    // InternalMyDsl.g:438:3: ( 'MOZAIC' )
                    {
                     before(grammarAccess.getDisplayItemChoiceEnumAccess().getMOZAICEnumLiteralDeclaration_1()); 
                    // InternalMyDsl.g:439:3: ( 'MOZAIC' )
                    // InternalMyDsl.g:439:4: 'MOZAIC'
                    {
                    match(input,14,FOLLOW_2); 

                    }

                     after(grammarAccess.getDisplayItemChoiceEnumAccess().getMOZAICEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayItemChoiceEnum__Alternatives"


    // $ANTLR start "rule__Site__Group__0"
    // InternalMyDsl.g:447:1: rule__Site__Group__0 : rule__Site__Group__0__Impl rule__Site__Group__1 ;
    public final void rule__Site__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:451:1: ( rule__Site__Group__0__Impl rule__Site__Group__1 )
            // InternalMyDsl.g:452:2: rule__Site__Group__0__Impl rule__Site__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Site__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__0"


    // $ANTLR start "rule__Site__Group__0__Impl"
    // InternalMyDsl.g:459:1: rule__Site__Group__0__Impl : ( () ) ;
    public final void rule__Site__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:463:1: ( ( () ) )
            // InternalMyDsl.g:464:1: ( () )
            {
            // InternalMyDsl.g:464:1: ( () )
            // InternalMyDsl.g:465:2: ()
            {
             before(grammarAccess.getSiteAccess().getSiteAction_0()); 
            // InternalMyDsl.g:466:2: ()
            // InternalMyDsl.g:466:3: 
            {
            }

             after(grammarAccess.getSiteAccess().getSiteAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__0__Impl"


    // $ANTLR start "rule__Site__Group__1"
    // InternalMyDsl.g:474:1: rule__Site__Group__1 : rule__Site__Group__1__Impl rule__Site__Group__2 ;
    public final void rule__Site__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:478:1: ( rule__Site__Group__1__Impl rule__Site__Group__2 )
            // InternalMyDsl.g:479:2: rule__Site__Group__1__Impl rule__Site__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Site__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__1"


    // $ANTLR start "rule__Site__Group__1__Impl"
    // InternalMyDsl.g:486:1: rule__Site__Group__1__Impl : ( 'site' ) ;
    public final void rule__Site__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:490:1: ( ( 'site' ) )
            // InternalMyDsl.g:491:1: ( 'site' )
            {
            // InternalMyDsl.g:491:1: ( 'site' )
            // InternalMyDsl.g:492:2: 'site'
            {
             before(grammarAccess.getSiteAccess().getSiteKeyword_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getSiteAccess().getSiteKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__1__Impl"


    // $ANTLR start "rule__Site__Group__2"
    // InternalMyDsl.g:501:1: rule__Site__Group__2 : rule__Site__Group__2__Impl rule__Site__Group__3 ;
    public final void rule__Site__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:505:1: ( rule__Site__Group__2__Impl rule__Site__Group__3 )
            // InternalMyDsl.g:506:2: rule__Site__Group__2__Impl rule__Site__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Site__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__2"


    // $ANTLR start "rule__Site__Group__2__Impl"
    // InternalMyDsl.g:513:1: rule__Site__Group__2__Impl : ( ( rule__Site__NameAssignment_2 ) ) ;
    public final void rule__Site__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:517:1: ( ( ( rule__Site__NameAssignment_2 ) ) )
            // InternalMyDsl.g:518:1: ( ( rule__Site__NameAssignment_2 ) )
            {
            // InternalMyDsl.g:518:1: ( ( rule__Site__NameAssignment_2 ) )
            // InternalMyDsl.g:519:2: ( rule__Site__NameAssignment_2 )
            {
             before(grammarAccess.getSiteAccess().getNameAssignment_2()); 
            // InternalMyDsl.g:520:2: ( rule__Site__NameAssignment_2 )
            // InternalMyDsl.g:520:3: rule__Site__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Site__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSiteAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__2__Impl"


    // $ANTLR start "rule__Site__Group__3"
    // InternalMyDsl.g:528:1: rule__Site__Group__3 : rule__Site__Group__3__Impl rule__Site__Group__4 ;
    public final void rule__Site__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:532:1: ( rule__Site__Group__3__Impl rule__Site__Group__4 )
            // InternalMyDsl.g:533:2: rule__Site__Group__3__Impl rule__Site__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Site__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__3"


    // $ANTLR start "rule__Site__Group__3__Impl"
    // InternalMyDsl.g:540:1: rule__Site__Group__3__Impl : ( ( rule__Site__CompanyAssignment_3 ) ) ;
    public final void rule__Site__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:544:1: ( ( ( rule__Site__CompanyAssignment_3 ) ) )
            // InternalMyDsl.g:545:1: ( ( rule__Site__CompanyAssignment_3 ) )
            {
            // InternalMyDsl.g:545:1: ( ( rule__Site__CompanyAssignment_3 ) )
            // InternalMyDsl.g:546:2: ( rule__Site__CompanyAssignment_3 )
            {
             before(grammarAccess.getSiteAccess().getCompanyAssignment_3()); 
            // InternalMyDsl.g:547:2: ( rule__Site__CompanyAssignment_3 )
            // InternalMyDsl.g:547:3: rule__Site__CompanyAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Site__CompanyAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getSiteAccess().getCompanyAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__3__Impl"


    // $ANTLR start "rule__Site__Group__4"
    // InternalMyDsl.g:555:1: rule__Site__Group__4 : rule__Site__Group__4__Impl rule__Site__Group__5 ;
    public final void rule__Site__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:559:1: ( rule__Site__Group__4__Impl rule__Site__Group__5 )
            // InternalMyDsl.g:560:2: rule__Site__Group__4__Impl rule__Site__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Site__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__4"


    // $ANTLR start "rule__Site__Group__4__Impl"
    // InternalMyDsl.g:567:1: rule__Site__Group__4__Impl : ( 'apis' ) ;
    public final void rule__Site__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:571:1: ( ( 'apis' ) )
            // InternalMyDsl.g:572:1: ( 'apis' )
            {
            // InternalMyDsl.g:572:1: ( 'apis' )
            // InternalMyDsl.g:573:2: 'apis'
            {
             before(grammarAccess.getSiteAccess().getApisKeyword_4()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getSiteAccess().getApisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__4__Impl"


    // $ANTLR start "rule__Site__Group__5"
    // InternalMyDsl.g:582:1: rule__Site__Group__5 : rule__Site__Group__5__Impl rule__Site__Group__6 ;
    public final void rule__Site__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:586:1: ( rule__Site__Group__5__Impl rule__Site__Group__6 )
            // InternalMyDsl.g:587:2: rule__Site__Group__5__Impl rule__Site__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__Site__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__5"


    // $ANTLR start "rule__Site__Group__5__Impl"
    // InternalMyDsl.g:594:1: rule__Site__Group__5__Impl : ( ( rule__Site__ApiAssignment_5 ) ) ;
    public final void rule__Site__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:598:1: ( ( ( rule__Site__ApiAssignment_5 ) ) )
            // InternalMyDsl.g:599:1: ( ( rule__Site__ApiAssignment_5 ) )
            {
            // InternalMyDsl.g:599:1: ( ( rule__Site__ApiAssignment_5 ) )
            // InternalMyDsl.g:600:2: ( rule__Site__ApiAssignment_5 )
            {
             before(grammarAccess.getSiteAccess().getApiAssignment_5()); 
            // InternalMyDsl.g:601:2: ( rule__Site__ApiAssignment_5 )
            // InternalMyDsl.g:601:3: rule__Site__ApiAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Site__ApiAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getSiteAccess().getApiAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__5__Impl"


    // $ANTLR start "rule__Site__Group__6"
    // InternalMyDsl.g:609:1: rule__Site__Group__6 : rule__Site__Group__6__Impl rule__Site__Group__7 ;
    public final void rule__Site__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:613:1: ( rule__Site__Group__6__Impl rule__Site__Group__7 )
            // InternalMyDsl.g:614:2: rule__Site__Group__6__Impl rule__Site__Group__7
            {
            pushFollow(FOLLOW_8);
            rule__Site__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__6"


    // $ANTLR start "rule__Site__Group__6__Impl"
    // InternalMyDsl.g:621:1: rule__Site__Group__6__Impl : ( ( rule__Site__Group_6__0 )* ) ;
    public final void rule__Site__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:625:1: ( ( ( rule__Site__Group_6__0 )* ) )
            // InternalMyDsl.g:626:1: ( ( rule__Site__Group_6__0 )* )
            {
            // InternalMyDsl.g:626:1: ( ( rule__Site__Group_6__0 )* )
            // InternalMyDsl.g:627:2: ( rule__Site__Group_6__0 )*
            {
             before(grammarAccess.getSiteAccess().getGroup_6()); 
            // InternalMyDsl.g:628:2: ( rule__Site__Group_6__0 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==18) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMyDsl.g:628:3: rule__Site__Group_6__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Site__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getSiteAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__6__Impl"


    // $ANTLR start "rule__Site__Group__7"
    // InternalMyDsl.g:636:1: rule__Site__Group__7 : rule__Site__Group__7__Impl rule__Site__Group__8 ;
    public final void rule__Site__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:640:1: ( rule__Site__Group__7__Impl rule__Site__Group__8 )
            // InternalMyDsl.g:641:2: rule__Site__Group__7__Impl rule__Site__Group__8
            {
            pushFollow(FOLLOW_4);
            rule__Site__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__7"


    // $ANTLR start "rule__Site__Group__7__Impl"
    // InternalMyDsl.g:648:1: rule__Site__Group__7__Impl : ( 'tags' ) ;
    public final void rule__Site__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:652:1: ( ( 'tags' ) )
            // InternalMyDsl.g:653:1: ( 'tags' )
            {
            // InternalMyDsl.g:653:1: ( 'tags' )
            // InternalMyDsl.g:654:2: 'tags'
            {
             before(grammarAccess.getSiteAccess().getTagsKeyword_7()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSiteAccess().getTagsKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__7__Impl"


    // $ANTLR start "rule__Site__Group__8"
    // InternalMyDsl.g:663:1: rule__Site__Group__8 : rule__Site__Group__8__Impl rule__Site__Group__9 ;
    public final void rule__Site__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:667:1: ( rule__Site__Group__8__Impl rule__Site__Group__9 )
            // InternalMyDsl.g:668:2: rule__Site__Group__8__Impl rule__Site__Group__9
            {
            pushFollow(FOLLOW_10);
            rule__Site__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__8"


    // $ANTLR start "rule__Site__Group__8__Impl"
    // InternalMyDsl.g:675:1: rule__Site__Group__8__Impl : ( ( rule__Site__TagAssignment_8 ) ) ;
    public final void rule__Site__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:679:1: ( ( ( rule__Site__TagAssignment_8 ) ) )
            // InternalMyDsl.g:680:1: ( ( rule__Site__TagAssignment_8 ) )
            {
            // InternalMyDsl.g:680:1: ( ( rule__Site__TagAssignment_8 ) )
            // InternalMyDsl.g:681:2: ( rule__Site__TagAssignment_8 )
            {
             before(grammarAccess.getSiteAccess().getTagAssignment_8()); 
            // InternalMyDsl.g:682:2: ( rule__Site__TagAssignment_8 )
            // InternalMyDsl.g:682:3: rule__Site__TagAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__Site__TagAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getSiteAccess().getTagAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__8__Impl"


    // $ANTLR start "rule__Site__Group__9"
    // InternalMyDsl.g:690:1: rule__Site__Group__9 : rule__Site__Group__9__Impl rule__Site__Group__10 ;
    public final void rule__Site__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:694:1: ( rule__Site__Group__9__Impl rule__Site__Group__10 )
            // InternalMyDsl.g:695:2: rule__Site__Group__9__Impl rule__Site__Group__10
            {
            pushFollow(FOLLOW_10);
            rule__Site__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__9"


    // $ANTLR start "rule__Site__Group__9__Impl"
    // InternalMyDsl.g:702:1: rule__Site__Group__9__Impl : ( ( rule__Site__Group_9__0 )* ) ;
    public final void rule__Site__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:706:1: ( ( ( rule__Site__Group_9__0 )* ) )
            // InternalMyDsl.g:707:1: ( ( rule__Site__Group_9__0 )* )
            {
            // InternalMyDsl.g:707:1: ( ( rule__Site__Group_9__0 )* )
            // InternalMyDsl.g:708:2: ( rule__Site__Group_9__0 )*
            {
             before(grammarAccess.getSiteAccess().getGroup_9()); 
            // InternalMyDsl.g:709:2: ( rule__Site__Group_9__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==18) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:709:3: rule__Site__Group_9__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Site__Group_9__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getSiteAccess().getGroup_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__9__Impl"


    // $ANTLR start "rule__Site__Group__10"
    // InternalMyDsl.g:717:1: rule__Site__Group__10 : rule__Site__Group__10__Impl rule__Site__Group__11 ;
    public final void rule__Site__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:721:1: ( rule__Site__Group__10__Impl rule__Site__Group__11 )
            // InternalMyDsl.g:722:2: rule__Site__Group__10__Impl rule__Site__Group__11
            {
            pushFollow(FOLLOW_10);
            rule__Site__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__10"


    // $ANTLR start "rule__Site__Group__10__Impl"
    // InternalMyDsl.g:729:1: rule__Site__Group__10__Impl : ( ( rule__Site__Group_10__0 )? ) ;
    public final void rule__Site__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:733:1: ( ( ( rule__Site__Group_10__0 )? ) )
            // InternalMyDsl.g:734:1: ( ( rule__Site__Group_10__0 )? )
            {
            // InternalMyDsl.g:734:1: ( ( rule__Site__Group_10__0 )? )
            // InternalMyDsl.g:735:2: ( rule__Site__Group_10__0 )?
            {
             before(grammarAccess.getSiteAccess().getGroup_10()); 
            // InternalMyDsl.g:736:2: ( rule__Site__Group_10__0 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==19) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalMyDsl.g:736:3: rule__Site__Group_10__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Site__Group_10__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSiteAccess().getGroup_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__10__Impl"


    // $ANTLR start "rule__Site__Group__11"
    // InternalMyDsl.g:744:1: rule__Site__Group__11 : rule__Site__Group__11__Impl ;
    public final void rule__Site__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:748:1: ( rule__Site__Group__11__Impl )
            // InternalMyDsl.g:749:2: rule__Site__Group__11__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Site__Group__11__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__11"


    // $ANTLR start "rule__Site__Group__11__Impl"
    // InternalMyDsl.g:755:1: rule__Site__Group__11__Impl : ( ( rule__Site__Group_11__0 )? ) ;
    public final void rule__Site__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:759:1: ( ( ( rule__Site__Group_11__0 )? ) )
            // InternalMyDsl.g:760:1: ( ( rule__Site__Group_11__0 )? )
            {
            // InternalMyDsl.g:760:1: ( ( rule__Site__Group_11__0 )? )
            // InternalMyDsl.g:761:2: ( rule__Site__Group_11__0 )?
            {
             before(grammarAccess.getSiteAccess().getGroup_11()); 
            // InternalMyDsl.g:762:2: ( rule__Site__Group_11__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==20) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalMyDsl.g:762:3: rule__Site__Group_11__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Site__Group_11__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSiteAccess().getGroup_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group__11__Impl"


    // $ANTLR start "rule__Site__Group_6__0"
    // InternalMyDsl.g:771:1: rule__Site__Group_6__0 : rule__Site__Group_6__0__Impl rule__Site__Group_6__1 ;
    public final void rule__Site__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:775:1: ( rule__Site__Group_6__0__Impl rule__Site__Group_6__1 )
            // InternalMyDsl.g:776:2: rule__Site__Group_6__0__Impl rule__Site__Group_6__1
            {
            pushFollow(FOLLOW_7);
            rule__Site__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_6__0"


    // $ANTLR start "rule__Site__Group_6__0__Impl"
    // InternalMyDsl.g:783:1: rule__Site__Group_6__0__Impl : ( ',' ) ;
    public final void rule__Site__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:787:1: ( ( ',' ) )
            // InternalMyDsl.g:788:1: ( ',' )
            {
            // InternalMyDsl.g:788:1: ( ',' )
            // InternalMyDsl.g:789:2: ','
            {
             before(grammarAccess.getSiteAccess().getCommaKeyword_6_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSiteAccess().getCommaKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_6__0__Impl"


    // $ANTLR start "rule__Site__Group_6__1"
    // InternalMyDsl.g:798:1: rule__Site__Group_6__1 : rule__Site__Group_6__1__Impl ;
    public final void rule__Site__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:802:1: ( rule__Site__Group_6__1__Impl )
            // InternalMyDsl.g:803:2: rule__Site__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Site__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_6__1"


    // $ANTLR start "rule__Site__Group_6__1__Impl"
    // InternalMyDsl.g:809:1: rule__Site__Group_6__1__Impl : ( ( rule__Site__ApiAssignment_6_1 ) ) ;
    public final void rule__Site__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:813:1: ( ( ( rule__Site__ApiAssignment_6_1 ) ) )
            // InternalMyDsl.g:814:1: ( ( rule__Site__ApiAssignment_6_1 ) )
            {
            // InternalMyDsl.g:814:1: ( ( rule__Site__ApiAssignment_6_1 ) )
            // InternalMyDsl.g:815:2: ( rule__Site__ApiAssignment_6_1 )
            {
             before(grammarAccess.getSiteAccess().getApiAssignment_6_1()); 
            // InternalMyDsl.g:816:2: ( rule__Site__ApiAssignment_6_1 )
            // InternalMyDsl.g:816:3: rule__Site__ApiAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Site__ApiAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getSiteAccess().getApiAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_6__1__Impl"


    // $ANTLR start "rule__Site__Group_9__0"
    // InternalMyDsl.g:825:1: rule__Site__Group_9__0 : rule__Site__Group_9__0__Impl rule__Site__Group_9__1 ;
    public final void rule__Site__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:829:1: ( rule__Site__Group_9__0__Impl rule__Site__Group_9__1 )
            // InternalMyDsl.g:830:2: rule__Site__Group_9__0__Impl rule__Site__Group_9__1
            {
            pushFollow(FOLLOW_4);
            rule__Site__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_9__0"


    // $ANTLR start "rule__Site__Group_9__0__Impl"
    // InternalMyDsl.g:837:1: rule__Site__Group_9__0__Impl : ( ',' ) ;
    public final void rule__Site__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:841:1: ( ( ',' ) )
            // InternalMyDsl.g:842:1: ( ',' )
            {
            // InternalMyDsl.g:842:1: ( ',' )
            // InternalMyDsl.g:843:2: ','
            {
             before(grammarAccess.getSiteAccess().getCommaKeyword_9_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSiteAccess().getCommaKeyword_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_9__0__Impl"


    // $ANTLR start "rule__Site__Group_9__1"
    // InternalMyDsl.g:852:1: rule__Site__Group_9__1 : rule__Site__Group_9__1__Impl ;
    public final void rule__Site__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:856:1: ( rule__Site__Group_9__1__Impl )
            // InternalMyDsl.g:857:2: rule__Site__Group_9__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Site__Group_9__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_9__1"


    // $ANTLR start "rule__Site__Group_9__1__Impl"
    // InternalMyDsl.g:863:1: rule__Site__Group_9__1__Impl : ( ( rule__Site__TagAssignment_9_1 ) ) ;
    public final void rule__Site__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:867:1: ( ( ( rule__Site__TagAssignment_9_1 ) ) )
            // InternalMyDsl.g:868:1: ( ( rule__Site__TagAssignment_9_1 ) )
            {
            // InternalMyDsl.g:868:1: ( ( rule__Site__TagAssignment_9_1 ) )
            // InternalMyDsl.g:869:2: ( rule__Site__TagAssignment_9_1 )
            {
             before(grammarAccess.getSiteAccess().getTagAssignment_9_1()); 
            // InternalMyDsl.g:870:2: ( rule__Site__TagAssignment_9_1 )
            // InternalMyDsl.g:870:3: rule__Site__TagAssignment_9_1
            {
            pushFollow(FOLLOW_2);
            rule__Site__TagAssignment_9_1();

            state._fsp--;


            }

             after(grammarAccess.getSiteAccess().getTagAssignment_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_9__1__Impl"


    // $ANTLR start "rule__Site__Group_10__0"
    // InternalMyDsl.g:879:1: rule__Site__Group_10__0 : rule__Site__Group_10__0__Impl rule__Site__Group_10__1 ;
    public final void rule__Site__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:883:1: ( rule__Site__Group_10__0__Impl rule__Site__Group_10__1 )
            // InternalMyDsl.g:884:2: rule__Site__Group_10__0__Impl rule__Site__Group_10__1
            {
            pushFollow(FOLLOW_11);
            rule__Site__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_10__0"


    // $ANTLR start "rule__Site__Group_10__0__Impl"
    // InternalMyDsl.g:891:1: rule__Site__Group_10__0__Impl : ( 'weather' ) ;
    public final void rule__Site__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:895:1: ( ( 'weather' ) )
            // InternalMyDsl.g:896:1: ( 'weather' )
            {
            // InternalMyDsl.g:896:1: ( 'weather' )
            // InternalMyDsl.g:897:2: 'weather'
            {
             before(grammarAccess.getSiteAccess().getWeatherKeyword_10_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSiteAccess().getWeatherKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_10__0__Impl"


    // $ANTLR start "rule__Site__Group_10__1"
    // InternalMyDsl.g:906:1: rule__Site__Group_10__1 : rule__Site__Group_10__1__Impl ;
    public final void rule__Site__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:910:1: ( rule__Site__Group_10__1__Impl )
            // InternalMyDsl.g:911:2: rule__Site__Group_10__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Site__Group_10__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_10__1"


    // $ANTLR start "rule__Site__Group_10__1__Impl"
    // InternalMyDsl.g:917:1: rule__Site__Group_10__1__Impl : ( ( rule__Site__WeatherfunctAssignment_10_1 ) ) ;
    public final void rule__Site__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:921:1: ( ( ( rule__Site__WeatherfunctAssignment_10_1 ) ) )
            // InternalMyDsl.g:922:1: ( ( rule__Site__WeatherfunctAssignment_10_1 ) )
            {
            // InternalMyDsl.g:922:1: ( ( rule__Site__WeatherfunctAssignment_10_1 ) )
            // InternalMyDsl.g:923:2: ( rule__Site__WeatherfunctAssignment_10_1 )
            {
             before(grammarAccess.getSiteAccess().getWeatherfunctAssignment_10_1()); 
            // InternalMyDsl.g:924:2: ( rule__Site__WeatherfunctAssignment_10_1 )
            // InternalMyDsl.g:924:3: rule__Site__WeatherfunctAssignment_10_1
            {
            pushFollow(FOLLOW_2);
            rule__Site__WeatherfunctAssignment_10_1();

            state._fsp--;


            }

             after(grammarAccess.getSiteAccess().getWeatherfunctAssignment_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_10__1__Impl"


    // $ANTLR start "rule__Site__Group_11__0"
    // InternalMyDsl.g:933:1: rule__Site__Group_11__0 : rule__Site__Group_11__0__Impl rule__Site__Group_11__1 ;
    public final void rule__Site__Group_11__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:937:1: ( rule__Site__Group_11__0__Impl rule__Site__Group_11__1 )
            // InternalMyDsl.g:938:2: rule__Site__Group_11__0__Impl rule__Site__Group_11__1
            {
            pushFollow(FOLLOW_12);
            rule__Site__Group_11__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group_11__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__0"


    // $ANTLR start "rule__Site__Group_11__0__Impl"
    // InternalMyDsl.g:945:1: rule__Site__Group_11__0__Impl : ( 'settings :' ) ;
    public final void rule__Site__Group_11__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:949:1: ( ( 'settings :' ) )
            // InternalMyDsl.g:950:1: ( 'settings :' )
            {
            // InternalMyDsl.g:950:1: ( 'settings :' )
            // InternalMyDsl.g:951:2: 'settings :'
            {
             before(grammarAccess.getSiteAccess().getSettingsKeyword_11_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getSiteAccess().getSettingsKeyword_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__0__Impl"


    // $ANTLR start "rule__Site__Group_11__1"
    // InternalMyDsl.g:960:1: rule__Site__Group_11__1 : rule__Site__Group_11__1__Impl rule__Site__Group_11__2 ;
    public final void rule__Site__Group_11__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:964:1: ( rule__Site__Group_11__1__Impl rule__Site__Group_11__2 )
            // InternalMyDsl.g:965:2: rule__Site__Group_11__1__Impl rule__Site__Group_11__2
            {
            pushFollow(FOLLOW_12);
            rule__Site__Group_11__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group_11__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__1"


    // $ANTLR start "rule__Site__Group_11__1__Impl"
    // InternalMyDsl.g:972:1: rule__Site__Group_11__1__Impl : ( ( rule__Site__DisplayAssignment_11_1 )? ) ;
    public final void rule__Site__Group_11__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:976:1: ( ( ( rule__Site__DisplayAssignment_11_1 )? ) )
            // InternalMyDsl.g:977:1: ( ( rule__Site__DisplayAssignment_11_1 )? )
            {
            // InternalMyDsl.g:977:1: ( ( rule__Site__DisplayAssignment_11_1 )? )
            // InternalMyDsl.g:978:2: ( rule__Site__DisplayAssignment_11_1 )?
            {
             before(grammarAccess.getSiteAccess().getDisplayAssignment_11_1()); 
            // InternalMyDsl.g:979:2: ( rule__Site__DisplayAssignment_11_1 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==24) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalMyDsl.g:979:3: rule__Site__DisplayAssignment_11_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Site__DisplayAssignment_11_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSiteAccess().getDisplayAssignment_11_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__1__Impl"


    // $ANTLR start "rule__Site__Group_11__2"
    // InternalMyDsl.g:987:1: rule__Site__Group_11__2 : rule__Site__Group_11__2__Impl rule__Site__Group_11__3 ;
    public final void rule__Site__Group_11__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:991:1: ( rule__Site__Group_11__2__Impl rule__Site__Group_11__3 )
            // InternalMyDsl.g:992:2: rule__Site__Group_11__2__Impl rule__Site__Group_11__3
            {
            pushFollow(FOLLOW_12);
            rule__Site__Group_11__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group_11__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__2"


    // $ANTLR start "rule__Site__Group_11__2__Impl"
    // InternalMyDsl.g:999:1: rule__Site__Group_11__2__Impl : ( ( rule__Site__DisplayAssignment_11_2 )? ) ;
    public final void rule__Site__Group_11__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1003:1: ( ( ( rule__Site__DisplayAssignment_11_2 )? ) )
            // InternalMyDsl.g:1004:1: ( ( rule__Site__DisplayAssignment_11_2 )? )
            {
            // InternalMyDsl.g:1004:1: ( ( rule__Site__DisplayAssignment_11_2 )? )
            // InternalMyDsl.g:1005:2: ( rule__Site__DisplayAssignment_11_2 )?
            {
             before(grammarAccess.getSiteAccess().getDisplayAssignment_11_2()); 
            // InternalMyDsl.g:1006:2: ( rule__Site__DisplayAssignment_11_2 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==26) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalMyDsl.g:1006:3: rule__Site__DisplayAssignment_11_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Site__DisplayAssignment_11_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSiteAccess().getDisplayAssignment_11_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__2__Impl"


    // $ANTLR start "rule__Site__Group_11__3"
    // InternalMyDsl.g:1014:1: rule__Site__Group_11__3 : rule__Site__Group_11__3__Impl rule__Site__Group_11__4 ;
    public final void rule__Site__Group_11__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1018:1: ( rule__Site__Group_11__3__Impl rule__Site__Group_11__4 )
            // InternalMyDsl.g:1019:2: rule__Site__Group_11__3__Impl rule__Site__Group_11__4
            {
            pushFollow(FOLLOW_12);
            rule__Site__Group_11__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group_11__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__3"


    // $ANTLR start "rule__Site__Group_11__3__Impl"
    // InternalMyDsl.g:1026:1: rule__Site__Group_11__3__Impl : ( ( rule__Site__DisplayAssignment_11_3 )? ) ;
    public final void rule__Site__Group_11__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1030:1: ( ( ( rule__Site__DisplayAssignment_11_3 )? ) )
            // InternalMyDsl.g:1031:1: ( ( rule__Site__DisplayAssignment_11_3 )? )
            {
            // InternalMyDsl.g:1031:1: ( ( rule__Site__DisplayAssignment_11_3 )? )
            // InternalMyDsl.g:1032:2: ( rule__Site__DisplayAssignment_11_3 )?
            {
             before(grammarAccess.getSiteAccess().getDisplayAssignment_11_3()); 
            // InternalMyDsl.g:1033:2: ( rule__Site__DisplayAssignment_11_3 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==28) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalMyDsl.g:1033:3: rule__Site__DisplayAssignment_11_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Site__DisplayAssignment_11_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSiteAccess().getDisplayAssignment_11_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__3__Impl"


    // $ANTLR start "rule__Site__Group_11__4"
    // InternalMyDsl.g:1041:1: rule__Site__Group_11__4 : rule__Site__Group_11__4__Impl rule__Site__Group_11__5 ;
    public final void rule__Site__Group_11__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1045:1: ( rule__Site__Group_11__4__Impl rule__Site__Group_11__5 )
            // InternalMyDsl.g:1046:2: rule__Site__Group_11__4__Impl rule__Site__Group_11__5
            {
            pushFollow(FOLLOW_12);
            rule__Site__Group_11__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Site__Group_11__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__4"


    // $ANTLR start "rule__Site__Group_11__4__Impl"
    // InternalMyDsl.g:1053:1: rule__Site__Group_11__4__Impl : ( ( rule__Site__DisplayAssignment_11_4 )? ) ;
    public final void rule__Site__Group_11__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1057:1: ( ( ( rule__Site__DisplayAssignment_11_4 )? ) )
            // InternalMyDsl.g:1058:1: ( ( rule__Site__DisplayAssignment_11_4 )? )
            {
            // InternalMyDsl.g:1058:1: ( ( rule__Site__DisplayAssignment_11_4 )? )
            // InternalMyDsl.g:1059:2: ( rule__Site__DisplayAssignment_11_4 )?
            {
             before(grammarAccess.getSiteAccess().getDisplayAssignment_11_4()); 
            // InternalMyDsl.g:1060:2: ( rule__Site__DisplayAssignment_11_4 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==29) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalMyDsl.g:1060:3: rule__Site__DisplayAssignment_11_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Site__DisplayAssignment_11_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSiteAccess().getDisplayAssignment_11_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__4__Impl"


    // $ANTLR start "rule__Site__Group_11__5"
    // InternalMyDsl.g:1068:1: rule__Site__Group_11__5 : rule__Site__Group_11__5__Impl ;
    public final void rule__Site__Group_11__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1072:1: ( rule__Site__Group_11__5__Impl )
            // InternalMyDsl.g:1073:2: rule__Site__Group_11__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Site__Group_11__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__5"


    // $ANTLR start "rule__Site__Group_11__5__Impl"
    // InternalMyDsl.g:1079:1: rule__Site__Group_11__5__Impl : ( ( rule__Site__DisplayAssignment_11_5 )? ) ;
    public final void rule__Site__Group_11__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1083:1: ( ( ( rule__Site__DisplayAssignment_11_5 )? ) )
            // InternalMyDsl.g:1084:1: ( ( rule__Site__DisplayAssignment_11_5 )? )
            {
            // InternalMyDsl.g:1084:1: ( ( rule__Site__DisplayAssignment_11_5 )? )
            // InternalMyDsl.g:1085:2: ( rule__Site__DisplayAssignment_11_5 )?
            {
             before(grammarAccess.getSiteAccess().getDisplayAssignment_11_5()); 
            // InternalMyDsl.g:1086:2: ( rule__Site__DisplayAssignment_11_5 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==30) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalMyDsl.g:1086:3: rule__Site__DisplayAssignment_11_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Site__DisplayAssignment_11_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSiteAccess().getDisplayAssignment_11_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__Group_11__5__Impl"


    // $ANTLR start "rule__Company__Group__0"
    // InternalMyDsl.g:1095:1: rule__Company__Group__0 : rule__Company__Group__0__Impl rule__Company__Group__1 ;
    public final void rule__Company__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1099:1: ( rule__Company__Group__0__Impl rule__Company__Group__1 )
            // InternalMyDsl.g:1100:2: rule__Company__Group__0__Impl rule__Company__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Company__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Company__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Company__Group__0"


    // $ANTLR start "rule__Company__Group__0__Impl"
    // InternalMyDsl.g:1107:1: rule__Company__Group__0__Impl : ( () ) ;
    public final void rule__Company__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1111:1: ( ( () ) )
            // InternalMyDsl.g:1112:1: ( () )
            {
            // InternalMyDsl.g:1112:1: ( () )
            // InternalMyDsl.g:1113:2: ()
            {
             before(grammarAccess.getCompanyAccess().getCompanyAction_0()); 
            // InternalMyDsl.g:1114:2: ()
            // InternalMyDsl.g:1114:3: 
            {
            }

             after(grammarAccess.getCompanyAccess().getCompanyAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Company__Group__0__Impl"


    // $ANTLR start "rule__Company__Group__1"
    // InternalMyDsl.g:1122:1: rule__Company__Group__1 : rule__Company__Group__1__Impl rule__Company__Group__2 ;
    public final void rule__Company__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1126:1: ( rule__Company__Group__1__Impl rule__Company__Group__2 )
            // InternalMyDsl.g:1127:2: rule__Company__Group__1__Impl rule__Company__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Company__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Company__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Company__Group__1"


    // $ANTLR start "rule__Company__Group__1__Impl"
    // InternalMyDsl.g:1134:1: rule__Company__Group__1__Impl : ( 'company' ) ;
    public final void rule__Company__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1138:1: ( ( 'company' ) )
            // InternalMyDsl.g:1139:1: ( 'company' )
            {
            // InternalMyDsl.g:1139:1: ( 'company' )
            // InternalMyDsl.g:1140:2: 'company'
            {
             before(grammarAccess.getCompanyAccess().getCompanyKeyword_1()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getCompanyAccess().getCompanyKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Company__Group__1__Impl"


    // $ANTLR start "rule__Company__Group__2"
    // InternalMyDsl.g:1149:1: rule__Company__Group__2 : rule__Company__Group__2__Impl ;
    public final void rule__Company__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1153:1: ( rule__Company__Group__2__Impl )
            // InternalMyDsl.g:1154:2: rule__Company__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Company__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Company__Group__2"


    // $ANTLR start "rule__Company__Group__2__Impl"
    // InternalMyDsl.g:1160:1: rule__Company__Group__2__Impl : ( ( rule__Company__NameAssignment_2 ) ) ;
    public final void rule__Company__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1164:1: ( ( ( rule__Company__NameAssignment_2 ) ) )
            // InternalMyDsl.g:1165:1: ( ( rule__Company__NameAssignment_2 ) )
            {
            // InternalMyDsl.g:1165:1: ( ( rule__Company__NameAssignment_2 ) )
            // InternalMyDsl.g:1166:2: ( rule__Company__NameAssignment_2 )
            {
             before(grammarAccess.getCompanyAccess().getNameAssignment_2()); 
            // InternalMyDsl.g:1167:2: ( rule__Company__NameAssignment_2 )
            // InternalMyDsl.g:1167:3: rule__Company__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Company__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCompanyAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Company__Group__2__Impl"


    // $ANTLR start "rule__Tag__Group__0"
    // InternalMyDsl.g:1176:1: rule__Tag__Group__0 : rule__Tag__Group__0__Impl rule__Tag__Group__1 ;
    public final void rule__Tag__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1180:1: ( rule__Tag__Group__0__Impl rule__Tag__Group__1 )
            // InternalMyDsl.g:1181:2: rule__Tag__Group__0__Impl rule__Tag__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Tag__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Tag__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__Group__0"


    // $ANTLR start "rule__Tag__Group__0__Impl"
    // InternalMyDsl.g:1188:1: rule__Tag__Group__0__Impl : ( () ) ;
    public final void rule__Tag__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1192:1: ( ( () ) )
            // InternalMyDsl.g:1193:1: ( () )
            {
            // InternalMyDsl.g:1193:1: ( () )
            // InternalMyDsl.g:1194:2: ()
            {
             before(grammarAccess.getTagAccess().getTagAction_0()); 
            // InternalMyDsl.g:1195:2: ()
            // InternalMyDsl.g:1195:3: 
            {
            }

             after(grammarAccess.getTagAccess().getTagAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__Group__0__Impl"


    // $ANTLR start "rule__Tag__Group__1"
    // InternalMyDsl.g:1203:1: rule__Tag__Group__1 : rule__Tag__Group__1__Impl ;
    public final void rule__Tag__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1207:1: ( rule__Tag__Group__1__Impl )
            // InternalMyDsl.g:1208:2: rule__Tag__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Tag__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__Group__1"


    // $ANTLR start "rule__Tag__Group__1__Impl"
    // InternalMyDsl.g:1214:1: rule__Tag__Group__1__Impl : ( ( rule__Tag__NameAssignment_1 ) ) ;
    public final void rule__Tag__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1218:1: ( ( ( rule__Tag__NameAssignment_1 ) ) )
            // InternalMyDsl.g:1219:1: ( ( rule__Tag__NameAssignment_1 ) )
            {
            // InternalMyDsl.g:1219:1: ( ( rule__Tag__NameAssignment_1 ) )
            // InternalMyDsl.g:1220:2: ( rule__Tag__NameAssignment_1 )
            {
             before(grammarAccess.getTagAccess().getNameAssignment_1()); 
            // InternalMyDsl.g:1221:2: ( rule__Tag__NameAssignment_1 )
            // InternalMyDsl.g:1221:3: rule__Tag__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Tag__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTagAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__Group__1__Impl"


    // $ANTLR start "rule__Api__Group__0"
    // InternalMyDsl.g:1230:1: rule__Api__Group__0 : rule__Api__Group__0__Impl rule__Api__Group__1 ;
    public final void rule__Api__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1234:1: ( rule__Api__Group__0__Impl rule__Api__Group__1 )
            // InternalMyDsl.g:1235:2: rule__Api__Group__0__Impl rule__Api__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Api__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Api__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Api__Group__0"


    // $ANTLR start "rule__Api__Group__0__Impl"
    // InternalMyDsl.g:1242:1: rule__Api__Group__0__Impl : ( () ) ;
    public final void rule__Api__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1246:1: ( ( () ) )
            // InternalMyDsl.g:1247:1: ( () )
            {
            // InternalMyDsl.g:1247:1: ( () )
            // InternalMyDsl.g:1248:2: ()
            {
             before(grammarAccess.getApiAccess().getApiAction_0()); 
            // InternalMyDsl.g:1249:2: ()
            // InternalMyDsl.g:1249:3: 
            {
            }

             after(grammarAccess.getApiAccess().getApiAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Api__Group__0__Impl"


    // $ANTLR start "rule__Api__Group__1"
    // InternalMyDsl.g:1257:1: rule__Api__Group__1 : rule__Api__Group__1__Impl ;
    public final void rule__Api__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1261:1: ( rule__Api__Group__1__Impl )
            // InternalMyDsl.g:1262:2: rule__Api__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Api__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Api__Group__1"


    // $ANTLR start "rule__Api__Group__1__Impl"
    // InternalMyDsl.g:1268:1: rule__Api__Group__1__Impl : ( ( rule__Api__ApiNameAssignment_1 ) ) ;
    public final void rule__Api__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1272:1: ( ( ( rule__Api__ApiNameAssignment_1 ) ) )
            // InternalMyDsl.g:1273:1: ( ( rule__Api__ApiNameAssignment_1 ) )
            {
            // InternalMyDsl.g:1273:1: ( ( rule__Api__ApiNameAssignment_1 ) )
            // InternalMyDsl.g:1274:2: ( rule__Api__ApiNameAssignment_1 )
            {
             before(grammarAccess.getApiAccess().getApiNameAssignment_1()); 
            // InternalMyDsl.g:1275:2: ( rule__Api__ApiNameAssignment_1 )
            // InternalMyDsl.g:1275:3: rule__Api__ApiNameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Api__ApiNameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getApiAccess().getApiNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Api__Group__1__Impl"


    // $ANTLR start "rule__WeatherFunct__Group__0"
    // InternalMyDsl.g:1284:1: rule__WeatherFunct__Group__0 : rule__WeatherFunct__Group__0__Impl rule__WeatherFunct__Group__1 ;
    public final void rule__WeatherFunct__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1288:1: ( rule__WeatherFunct__Group__0__Impl rule__WeatherFunct__Group__1 )
            // InternalMyDsl.g:1289:2: rule__WeatherFunct__Group__0__Impl rule__WeatherFunct__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__WeatherFunct__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WeatherFunct__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__Group__0"


    // $ANTLR start "rule__WeatherFunct__Group__0__Impl"
    // InternalMyDsl.g:1296:1: rule__WeatherFunct__Group__0__Impl : ( 'town' ) ;
    public final void rule__WeatherFunct__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1300:1: ( ( 'town' ) )
            // InternalMyDsl.g:1301:1: ( 'town' )
            {
            // InternalMyDsl.g:1301:1: ( 'town' )
            // InternalMyDsl.g:1302:2: 'town'
            {
             before(grammarAccess.getWeatherFunctAccess().getTownKeyword_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getWeatherFunctAccess().getTownKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__Group__0__Impl"


    // $ANTLR start "rule__WeatherFunct__Group__1"
    // InternalMyDsl.g:1311:1: rule__WeatherFunct__Group__1 : rule__WeatherFunct__Group__1__Impl rule__WeatherFunct__Group__2 ;
    public final void rule__WeatherFunct__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1315:1: ( rule__WeatherFunct__Group__1__Impl rule__WeatherFunct__Group__2 )
            // InternalMyDsl.g:1316:2: rule__WeatherFunct__Group__1__Impl rule__WeatherFunct__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__WeatherFunct__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WeatherFunct__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__Group__1"


    // $ANTLR start "rule__WeatherFunct__Group__1__Impl"
    // InternalMyDsl.g:1323:1: rule__WeatherFunct__Group__1__Impl : ( ( rule__WeatherFunct__TownAssignment_1 ) ) ;
    public final void rule__WeatherFunct__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1327:1: ( ( ( rule__WeatherFunct__TownAssignment_1 ) ) )
            // InternalMyDsl.g:1328:1: ( ( rule__WeatherFunct__TownAssignment_1 ) )
            {
            // InternalMyDsl.g:1328:1: ( ( rule__WeatherFunct__TownAssignment_1 ) )
            // InternalMyDsl.g:1329:2: ( rule__WeatherFunct__TownAssignment_1 )
            {
             before(grammarAccess.getWeatherFunctAccess().getTownAssignment_1()); 
            // InternalMyDsl.g:1330:2: ( rule__WeatherFunct__TownAssignment_1 )
            // InternalMyDsl.g:1330:3: rule__WeatherFunct__TownAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__WeatherFunct__TownAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWeatherFunctAccess().getTownAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__Group__1__Impl"


    // $ANTLR start "rule__WeatherFunct__Group__2"
    // InternalMyDsl.g:1338:1: rule__WeatherFunct__Group__2 : rule__WeatherFunct__Group__2__Impl rule__WeatherFunct__Group__3 ;
    public final void rule__WeatherFunct__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1342:1: ( rule__WeatherFunct__Group__2__Impl rule__WeatherFunct__Group__3 )
            // InternalMyDsl.g:1343:2: rule__WeatherFunct__Group__2__Impl rule__WeatherFunct__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__WeatherFunct__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WeatherFunct__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__Group__2"


    // $ANTLR start "rule__WeatherFunct__Group__2__Impl"
    // InternalMyDsl.g:1350:1: rule__WeatherFunct__Group__2__Impl : ( 'country' ) ;
    public final void rule__WeatherFunct__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1354:1: ( ( 'country' ) )
            // InternalMyDsl.g:1355:1: ( 'country' )
            {
            // InternalMyDsl.g:1355:1: ( 'country' )
            // InternalMyDsl.g:1356:2: 'country'
            {
             before(grammarAccess.getWeatherFunctAccess().getCountryKeyword_2()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getWeatherFunctAccess().getCountryKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__Group__2__Impl"


    // $ANTLR start "rule__WeatherFunct__Group__3"
    // InternalMyDsl.g:1365:1: rule__WeatherFunct__Group__3 : rule__WeatherFunct__Group__3__Impl ;
    public final void rule__WeatherFunct__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1369:1: ( rule__WeatherFunct__Group__3__Impl )
            // InternalMyDsl.g:1370:2: rule__WeatherFunct__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__WeatherFunct__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__Group__3"


    // $ANTLR start "rule__WeatherFunct__Group__3__Impl"
    // InternalMyDsl.g:1376:1: rule__WeatherFunct__Group__3__Impl : ( ( rule__WeatherFunct__CountryAssignment_3 ) ) ;
    public final void rule__WeatherFunct__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1380:1: ( ( ( rule__WeatherFunct__CountryAssignment_3 ) ) )
            // InternalMyDsl.g:1381:1: ( ( rule__WeatherFunct__CountryAssignment_3 ) )
            {
            // InternalMyDsl.g:1381:1: ( ( rule__WeatherFunct__CountryAssignment_3 ) )
            // InternalMyDsl.g:1382:2: ( rule__WeatherFunct__CountryAssignment_3 )
            {
             before(grammarAccess.getWeatherFunctAccess().getCountryAssignment_3()); 
            // InternalMyDsl.g:1383:2: ( rule__WeatherFunct__CountryAssignment_3 )
            // InternalMyDsl.g:1383:3: rule__WeatherFunct__CountryAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__WeatherFunct__CountryAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getWeatherFunctAccess().getCountryAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__Group__3__Impl"


    // $ANTLR start "rule__ItemNumberFunct__Group__0"
    // InternalMyDsl.g:1392:1: rule__ItemNumberFunct__Group__0 : rule__ItemNumberFunct__Group__0__Impl rule__ItemNumberFunct__Group__1 ;
    public final void rule__ItemNumberFunct__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1396:1: ( rule__ItemNumberFunct__Group__0__Impl rule__ItemNumberFunct__Group__1 )
            // InternalMyDsl.g:1397:2: rule__ItemNumberFunct__Group__0__Impl rule__ItemNumberFunct__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__ItemNumberFunct__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ItemNumberFunct__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__Group__0"


    // $ANTLR start "rule__ItemNumberFunct__Group__0__Impl"
    // InternalMyDsl.g:1404:1: rule__ItemNumberFunct__Group__0__Impl : ( () ) ;
    public final void rule__ItemNumberFunct__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1408:1: ( ( () ) )
            // InternalMyDsl.g:1409:1: ( () )
            {
            // InternalMyDsl.g:1409:1: ( () )
            // InternalMyDsl.g:1410:2: ()
            {
             before(grammarAccess.getItemNumberFunctAccess().getItemNumberFunctAction_0()); 
            // InternalMyDsl.g:1411:2: ()
            // InternalMyDsl.g:1411:3: 
            {
            }

             after(grammarAccess.getItemNumberFunctAccess().getItemNumberFunctAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__Group__0__Impl"


    // $ANTLR start "rule__ItemNumberFunct__Group__1"
    // InternalMyDsl.g:1419:1: rule__ItemNumberFunct__Group__1 : rule__ItemNumberFunct__Group__1__Impl rule__ItemNumberFunct__Group__2 ;
    public final void rule__ItemNumberFunct__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1423:1: ( rule__ItemNumberFunct__Group__1__Impl rule__ItemNumberFunct__Group__2 )
            // InternalMyDsl.g:1424:2: rule__ItemNumberFunct__Group__1__Impl rule__ItemNumberFunct__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__ItemNumberFunct__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ItemNumberFunct__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__Group__1"


    // $ANTLR start "rule__ItemNumberFunct__Group__1__Impl"
    // InternalMyDsl.g:1431:1: rule__ItemNumberFunct__Group__1__Impl : ( 'itemVideo' ) ;
    public final void rule__ItemNumberFunct__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1435:1: ( ( 'itemVideo' ) )
            // InternalMyDsl.g:1436:1: ( 'itemVideo' )
            {
            // InternalMyDsl.g:1436:1: ( 'itemVideo' )
            // InternalMyDsl.g:1437:2: 'itemVideo'
            {
             before(grammarAccess.getItemNumberFunctAccess().getItemVideoKeyword_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getItemNumberFunctAccess().getItemVideoKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__Group__1__Impl"


    // $ANTLR start "rule__ItemNumberFunct__Group__2"
    // InternalMyDsl.g:1446:1: rule__ItemNumberFunct__Group__2 : rule__ItemNumberFunct__Group__2__Impl rule__ItemNumberFunct__Group__3 ;
    public final void rule__ItemNumberFunct__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1450:1: ( rule__ItemNumberFunct__Group__2__Impl rule__ItemNumberFunct__Group__3 )
            // InternalMyDsl.g:1451:2: rule__ItemNumberFunct__Group__2__Impl rule__ItemNumberFunct__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__ItemNumberFunct__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ItemNumberFunct__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__Group__2"


    // $ANTLR start "rule__ItemNumberFunct__Group__2__Impl"
    // InternalMyDsl.g:1458:1: rule__ItemNumberFunct__Group__2__Impl : ( 'number' ) ;
    public final void rule__ItemNumberFunct__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1462:1: ( ( 'number' ) )
            // InternalMyDsl.g:1463:1: ( 'number' )
            {
            // InternalMyDsl.g:1463:1: ( 'number' )
            // InternalMyDsl.g:1464:2: 'number'
            {
             before(grammarAccess.getItemNumberFunctAccess().getNumberKeyword_2()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getItemNumberFunctAccess().getNumberKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__Group__2__Impl"


    // $ANTLR start "rule__ItemNumberFunct__Group__3"
    // InternalMyDsl.g:1473:1: rule__ItemNumberFunct__Group__3 : rule__ItemNumberFunct__Group__3__Impl ;
    public final void rule__ItemNumberFunct__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1477:1: ( rule__ItemNumberFunct__Group__3__Impl )
            // InternalMyDsl.g:1478:2: rule__ItemNumberFunct__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ItemNumberFunct__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__Group__3"


    // $ANTLR start "rule__ItemNumberFunct__Group__3__Impl"
    // InternalMyDsl.g:1484:1: rule__ItemNumberFunct__Group__3__Impl : ( ( rule__ItemNumberFunct__NumberAssignment_3 ) ) ;
    public final void rule__ItemNumberFunct__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1488:1: ( ( ( rule__ItemNumberFunct__NumberAssignment_3 ) ) )
            // InternalMyDsl.g:1489:1: ( ( rule__ItemNumberFunct__NumberAssignment_3 ) )
            {
            // InternalMyDsl.g:1489:1: ( ( rule__ItemNumberFunct__NumberAssignment_3 ) )
            // InternalMyDsl.g:1490:2: ( rule__ItemNumberFunct__NumberAssignment_3 )
            {
             before(grammarAccess.getItemNumberFunctAccess().getNumberAssignment_3()); 
            // InternalMyDsl.g:1491:2: ( rule__ItemNumberFunct__NumberAssignment_3 )
            // InternalMyDsl.g:1491:3: rule__ItemNumberFunct__NumberAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ItemNumberFunct__NumberAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getItemNumberFunctAccess().getNumberAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__Group__3__Impl"


    // $ANTLR start "rule__ColorPickerFunct__Group__0"
    // InternalMyDsl.g:1500:1: rule__ColorPickerFunct__Group__0 : rule__ColorPickerFunct__Group__0__Impl rule__ColorPickerFunct__Group__1 ;
    public final void rule__ColorPickerFunct__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1504:1: ( rule__ColorPickerFunct__Group__0__Impl rule__ColorPickerFunct__Group__1 )
            // InternalMyDsl.g:1505:2: rule__ColorPickerFunct__Group__0__Impl rule__ColorPickerFunct__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__ColorPickerFunct__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ColorPickerFunct__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__Group__0"


    // $ANTLR start "rule__ColorPickerFunct__Group__0__Impl"
    // InternalMyDsl.g:1512:1: rule__ColorPickerFunct__Group__0__Impl : ( () ) ;
    public final void rule__ColorPickerFunct__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1516:1: ( ( () ) )
            // InternalMyDsl.g:1517:1: ( () )
            {
            // InternalMyDsl.g:1517:1: ( () )
            // InternalMyDsl.g:1518:2: ()
            {
             before(grammarAccess.getColorPickerFunctAccess().getColorPickerFunctAction_0()); 
            // InternalMyDsl.g:1519:2: ()
            // InternalMyDsl.g:1519:3: 
            {
            }

             after(grammarAccess.getColorPickerFunctAccess().getColorPickerFunctAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__Group__0__Impl"


    // $ANTLR start "rule__ColorPickerFunct__Group__1"
    // InternalMyDsl.g:1527:1: rule__ColorPickerFunct__Group__1 : rule__ColorPickerFunct__Group__1__Impl rule__ColorPickerFunct__Group__2 ;
    public final void rule__ColorPickerFunct__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1531:1: ( rule__ColorPickerFunct__Group__1__Impl rule__ColorPickerFunct__Group__2 )
            // InternalMyDsl.g:1532:2: rule__ColorPickerFunct__Group__1__Impl rule__ColorPickerFunct__Group__2
            {
            pushFollow(FOLLOW_18);
            rule__ColorPickerFunct__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ColorPickerFunct__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__Group__1"


    // $ANTLR start "rule__ColorPickerFunct__Group__1__Impl"
    // InternalMyDsl.g:1539:1: rule__ColorPickerFunct__Group__1__Impl : ( 'colorPicker' ) ;
    public final void rule__ColorPickerFunct__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1543:1: ( ( 'colorPicker' ) )
            // InternalMyDsl.g:1544:1: ( 'colorPicker' )
            {
            // InternalMyDsl.g:1544:1: ( 'colorPicker' )
            // InternalMyDsl.g:1545:2: 'colorPicker'
            {
             before(grammarAccess.getColorPickerFunctAccess().getColorPickerKeyword_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getColorPickerFunctAccess().getColorPickerKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__Group__1__Impl"


    // $ANTLR start "rule__ColorPickerFunct__Group__2"
    // InternalMyDsl.g:1554:1: rule__ColorPickerFunct__Group__2 : rule__ColorPickerFunct__Group__2__Impl rule__ColorPickerFunct__Group__3 ;
    public final void rule__ColorPickerFunct__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1558:1: ( rule__ColorPickerFunct__Group__2__Impl rule__ColorPickerFunct__Group__3 )
            // InternalMyDsl.g:1559:2: rule__ColorPickerFunct__Group__2__Impl rule__ColorPickerFunct__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__ColorPickerFunct__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ColorPickerFunct__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__Group__2"


    // $ANTLR start "rule__ColorPickerFunct__Group__2__Impl"
    // InternalMyDsl.g:1566:1: rule__ColorPickerFunct__Group__2__Impl : ( 'default' ) ;
    public final void rule__ColorPickerFunct__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1570:1: ( ( 'default' ) )
            // InternalMyDsl.g:1571:1: ( 'default' )
            {
            // InternalMyDsl.g:1571:1: ( 'default' )
            // InternalMyDsl.g:1572:2: 'default'
            {
             before(grammarAccess.getColorPickerFunctAccess().getDefaultKeyword_2()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getColorPickerFunctAccess().getDefaultKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__Group__2__Impl"


    // $ANTLR start "rule__ColorPickerFunct__Group__3"
    // InternalMyDsl.g:1581:1: rule__ColorPickerFunct__Group__3 : rule__ColorPickerFunct__Group__3__Impl ;
    public final void rule__ColorPickerFunct__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1585:1: ( rule__ColorPickerFunct__Group__3__Impl )
            // InternalMyDsl.g:1586:2: rule__ColorPickerFunct__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ColorPickerFunct__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__Group__3"


    // $ANTLR start "rule__ColorPickerFunct__Group__3__Impl"
    // InternalMyDsl.g:1592:1: rule__ColorPickerFunct__Group__3__Impl : ( ( rule__ColorPickerFunct__DefaultColorAssignment_3 ) ) ;
    public final void rule__ColorPickerFunct__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1596:1: ( ( ( rule__ColorPickerFunct__DefaultColorAssignment_3 ) ) )
            // InternalMyDsl.g:1597:1: ( ( rule__ColorPickerFunct__DefaultColorAssignment_3 ) )
            {
            // InternalMyDsl.g:1597:1: ( ( rule__ColorPickerFunct__DefaultColorAssignment_3 ) )
            // InternalMyDsl.g:1598:2: ( rule__ColorPickerFunct__DefaultColorAssignment_3 )
            {
             before(grammarAccess.getColorPickerFunctAccess().getDefaultColorAssignment_3()); 
            // InternalMyDsl.g:1599:2: ( rule__ColorPickerFunct__DefaultColorAssignment_3 )
            // InternalMyDsl.g:1599:3: rule__ColorPickerFunct__DefaultColorAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ColorPickerFunct__DefaultColorAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getColorPickerFunctAccess().getDefaultColorAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__Group__3__Impl"


    // $ANTLR start "rule__DisplayItemFunct__Group__0"
    // InternalMyDsl.g:1608:1: rule__DisplayItemFunct__Group__0 : rule__DisplayItemFunct__Group__0__Impl rule__DisplayItemFunct__Group__1 ;
    public final void rule__DisplayItemFunct__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1612:1: ( rule__DisplayItemFunct__Group__0__Impl rule__DisplayItemFunct__Group__1 )
            // InternalMyDsl.g:1613:2: rule__DisplayItemFunct__Group__0__Impl rule__DisplayItemFunct__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__DisplayItemFunct__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DisplayItemFunct__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayItemFunct__Group__0"


    // $ANTLR start "rule__DisplayItemFunct__Group__0__Impl"
    // InternalMyDsl.g:1620:1: rule__DisplayItemFunct__Group__0__Impl : ( 'displayItem' ) ;
    public final void rule__DisplayItemFunct__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1624:1: ( ( 'displayItem' ) )
            // InternalMyDsl.g:1625:1: ( 'displayItem' )
            {
            // InternalMyDsl.g:1625:1: ( 'displayItem' )
            // InternalMyDsl.g:1626:2: 'displayItem'
            {
             before(grammarAccess.getDisplayItemFunctAccess().getDisplayItemKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getDisplayItemFunctAccess().getDisplayItemKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayItemFunct__Group__0__Impl"


    // $ANTLR start "rule__DisplayItemFunct__Group__1"
    // InternalMyDsl.g:1635:1: rule__DisplayItemFunct__Group__1 : rule__DisplayItemFunct__Group__1__Impl rule__DisplayItemFunct__Group__2 ;
    public final void rule__DisplayItemFunct__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1639:1: ( rule__DisplayItemFunct__Group__1__Impl rule__DisplayItemFunct__Group__2 )
            // InternalMyDsl.g:1640:2: rule__DisplayItemFunct__Group__1__Impl rule__DisplayItemFunct__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__DisplayItemFunct__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DisplayItemFunct__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayItemFunct__Group__1"


    // $ANTLR start "rule__DisplayItemFunct__Group__1__Impl"
    // InternalMyDsl.g:1647:1: rule__DisplayItemFunct__Group__1__Impl : ( 'default' ) ;
    public final void rule__DisplayItemFunct__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1651:1: ( ( 'default' ) )
            // InternalMyDsl.g:1652:1: ( 'default' )
            {
            // InternalMyDsl.g:1652:1: ( 'default' )
            // InternalMyDsl.g:1653:2: 'default'
            {
             before(grammarAccess.getDisplayItemFunctAccess().getDefaultKeyword_1()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getDisplayItemFunctAccess().getDefaultKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayItemFunct__Group__1__Impl"


    // $ANTLR start "rule__DisplayItemFunct__Group__2"
    // InternalMyDsl.g:1662:1: rule__DisplayItemFunct__Group__2 : rule__DisplayItemFunct__Group__2__Impl ;
    public final void rule__DisplayItemFunct__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1666:1: ( rule__DisplayItemFunct__Group__2__Impl )
            // InternalMyDsl.g:1667:2: rule__DisplayItemFunct__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DisplayItemFunct__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayItemFunct__Group__2"


    // $ANTLR start "rule__DisplayItemFunct__Group__2__Impl"
    // InternalMyDsl.g:1673:1: rule__DisplayItemFunct__Group__2__Impl : ( ( rule__DisplayItemFunct__DefaultDisplayItemAssignment_2 ) ) ;
    public final void rule__DisplayItemFunct__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1677:1: ( ( ( rule__DisplayItemFunct__DefaultDisplayItemAssignment_2 ) ) )
            // InternalMyDsl.g:1678:1: ( ( rule__DisplayItemFunct__DefaultDisplayItemAssignment_2 ) )
            {
            // InternalMyDsl.g:1678:1: ( ( rule__DisplayItemFunct__DefaultDisplayItemAssignment_2 ) )
            // InternalMyDsl.g:1679:2: ( rule__DisplayItemFunct__DefaultDisplayItemAssignment_2 )
            {
             before(grammarAccess.getDisplayItemFunctAccess().getDefaultDisplayItemAssignment_2()); 
            // InternalMyDsl.g:1680:2: ( rule__DisplayItemFunct__DefaultDisplayItemAssignment_2 )
            // InternalMyDsl.g:1680:3: rule__DisplayItemFunct__DefaultDisplayItemAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DisplayItemFunct__DefaultDisplayItemAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDisplayItemFunctAccess().getDefaultDisplayItemAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayItemFunct__Group__2__Impl"


    // $ANTLR start "rule__SearchBarFunct__Group__0"
    // InternalMyDsl.g:1689:1: rule__SearchBarFunct__Group__0 : rule__SearchBarFunct__Group__0__Impl rule__SearchBarFunct__Group__1 ;
    public final void rule__SearchBarFunct__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1693:1: ( rule__SearchBarFunct__Group__0__Impl rule__SearchBarFunct__Group__1 )
            // InternalMyDsl.g:1694:2: rule__SearchBarFunct__Group__0__Impl rule__SearchBarFunct__Group__1
            {
            pushFollow(FOLLOW_20);
            rule__SearchBarFunct__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SearchBarFunct__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SearchBarFunct__Group__0"


    // $ANTLR start "rule__SearchBarFunct__Group__0__Impl"
    // InternalMyDsl.g:1701:1: rule__SearchBarFunct__Group__0__Impl : ( () ) ;
    public final void rule__SearchBarFunct__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1705:1: ( ( () ) )
            // InternalMyDsl.g:1706:1: ( () )
            {
            // InternalMyDsl.g:1706:1: ( () )
            // InternalMyDsl.g:1707:2: ()
            {
             before(grammarAccess.getSearchBarFunctAccess().getSearchBarFunctAction_0()); 
            // InternalMyDsl.g:1708:2: ()
            // InternalMyDsl.g:1708:3: 
            {
            }

             after(grammarAccess.getSearchBarFunctAccess().getSearchBarFunctAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SearchBarFunct__Group__0__Impl"


    // $ANTLR start "rule__SearchBarFunct__Group__1"
    // InternalMyDsl.g:1716:1: rule__SearchBarFunct__Group__1 : rule__SearchBarFunct__Group__1__Impl ;
    public final void rule__SearchBarFunct__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1720:1: ( rule__SearchBarFunct__Group__1__Impl )
            // InternalMyDsl.g:1721:2: rule__SearchBarFunct__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SearchBarFunct__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SearchBarFunct__Group__1"


    // $ANTLR start "rule__SearchBarFunct__Group__1__Impl"
    // InternalMyDsl.g:1727:1: rule__SearchBarFunct__Group__1__Impl : ( 'searchBar' ) ;
    public final void rule__SearchBarFunct__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1731:1: ( ( 'searchBar' ) )
            // InternalMyDsl.g:1732:1: ( 'searchBar' )
            {
            // InternalMyDsl.g:1732:1: ( 'searchBar' )
            // InternalMyDsl.g:1733:2: 'searchBar'
            {
             before(grammarAccess.getSearchBarFunctAccess().getSearchBarKeyword_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getSearchBarFunctAccess().getSearchBarKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SearchBarFunct__Group__1__Impl"


    // $ANTLR start "rule__SortBarFunct__Group__0"
    // InternalMyDsl.g:1743:1: rule__SortBarFunct__Group__0 : rule__SortBarFunct__Group__0__Impl rule__SortBarFunct__Group__1 ;
    public final void rule__SortBarFunct__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1747:1: ( rule__SortBarFunct__Group__0__Impl rule__SortBarFunct__Group__1 )
            // InternalMyDsl.g:1748:2: rule__SortBarFunct__Group__0__Impl rule__SortBarFunct__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__SortBarFunct__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SortBarFunct__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SortBarFunct__Group__0"


    // $ANTLR start "rule__SortBarFunct__Group__0__Impl"
    // InternalMyDsl.g:1755:1: rule__SortBarFunct__Group__0__Impl : ( () ) ;
    public final void rule__SortBarFunct__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1759:1: ( ( () ) )
            // InternalMyDsl.g:1760:1: ( () )
            {
            // InternalMyDsl.g:1760:1: ( () )
            // InternalMyDsl.g:1761:2: ()
            {
             before(grammarAccess.getSortBarFunctAccess().getSortBarFunctAction_0()); 
            // InternalMyDsl.g:1762:2: ()
            // InternalMyDsl.g:1762:3: 
            {
            }

             after(grammarAccess.getSortBarFunctAccess().getSortBarFunctAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SortBarFunct__Group__0__Impl"


    // $ANTLR start "rule__SortBarFunct__Group__1"
    // InternalMyDsl.g:1770:1: rule__SortBarFunct__Group__1 : rule__SortBarFunct__Group__1__Impl ;
    public final void rule__SortBarFunct__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1774:1: ( rule__SortBarFunct__Group__1__Impl )
            // InternalMyDsl.g:1775:2: rule__SortBarFunct__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SortBarFunct__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SortBarFunct__Group__1"


    // $ANTLR start "rule__SortBarFunct__Group__1__Impl"
    // InternalMyDsl.g:1781:1: rule__SortBarFunct__Group__1__Impl : ( 'sortBar' ) ;
    public final void rule__SortBarFunct__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1785:1: ( ( 'sortBar' ) )
            // InternalMyDsl.g:1786:1: ( 'sortBar' )
            {
            // InternalMyDsl.g:1786:1: ( 'sortBar' )
            // InternalMyDsl.g:1787:2: 'sortBar'
            {
             before(grammarAccess.getSortBarFunctAccess().getSortBarKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getSortBarFunctAccess().getSortBarKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SortBarFunct__Group__1__Impl"


    // $ANTLR start "rule__Site__NameAssignment_2"
    // InternalMyDsl.g:1797:1: rule__Site__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Site__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1801:1: ( ( ruleEString ) )
            // InternalMyDsl.g:1802:2: ( ruleEString )
            {
            // InternalMyDsl.g:1802:2: ( ruleEString )
            // InternalMyDsl.g:1803:3: ruleEString
            {
             before(grammarAccess.getSiteAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__NameAssignment_2"


    // $ANTLR start "rule__Site__CompanyAssignment_3"
    // InternalMyDsl.g:1812:1: rule__Site__CompanyAssignment_3 : ( ruleCompany ) ;
    public final void rule__Site__CompanyAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1816:1: ( ( ruleCompany ) )
            // InternalMyDsl.g:1817:2: ( ruleCompany )
            {
            // InternalMyDsl.g:1817:2: ( ruleCompany )
            // InternalMyDsl.g:1818:3: ruleCompany
            {
             before(grammarAccess.getSiteAccess().getCompanyCompanyParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCompany();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getCompanyCompanyParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__CompanyAssignment_3"


    // $ANTLR start "rule__Site__ApiAssignment_5"
    // InternalMyDsl.g:1827:1: rule__Site__ApiAssignment_5 : ( ruleApi ) ;
    public final void rule__Site__ApiAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1831:1: ( ( ruleApi ) )
            // InternalMyDsl.g:1832:2: ( ruleApi )
            {
            // InternalMyDsl.g:1832:2: ( ruleApi )
            // InternalMyDsl.g:1833:3: ruleApi
            {
             before(grammarAccess.getSiteAccess().getApiApiParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleApi();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getApiApiParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__ApiAssignment_5"


    // $ANTLR start "rule__Site__ApiAssignment_6_1"
    // InternalMyDsl.g:1842:1: rule__Site__ApiAssignment_6_1 : ( ruleApi ) ;
    public final void rule__Site__ApiAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1846:1: ( ( ruleApi ) )
            // InternalMyDsl.g:1847:2: ( ruleApi )
            {
            // InternalMyDsl.g:1847:2: ( ruleApi )
            // InternalMyDsl.g:1848:3: ruleApi
            {
             before(grammarAccess.getSiteAccess().getApiApiParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleApi();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getApiApiParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__ApiAssignment_6_1"


    // $ANTLR start "rule__Site__TagAssignment_8"
    // InternalMyDsl.g:1857:1: rule__Site__TagAssignment_8 : ( ruleTag ) ;
    public final void rule__Site__TagAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1861:1: ( ( ruleTag ) )
            // InternalMyDsl.g:1862:2: ( ruleTag )
            {
            // InternalMyDsl.g:1862:2: ( ruleTag )
            // InternalMyDsl.g:1863:3: ruleTag
            {
             before(grammarAccess.getSiteAccess().getTagTagParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleTag();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getTagTagParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__TagAssignment_8"


    // $ANTLR start "rule__Site__TagAssignment_9_1"
    // InternalMyDsl.g:1872:1: rule__Site__TagAssignment_9_1 : ( ruleTag ) ;
    public final void rule__Site__TagAssignment_9_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1876:1: ( ( ruleTag ) )
            // InternalMyDsl.g:1877:2: ( ruleTag )
            {
            // InternalMyDsl.g:1877:2: ( ruleTag )
            // InternalMyDsl.g:1878:3: ruleTag
            {
             before(grammarAccess.getSiteAccess().getTagTagParserRuleCall_9_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTag();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getTagTagParserRuleCall_9_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__TagAssignment_9_1"


    // $ANTLR start "rule__Site__WeatherfunctAssignment_10_1"
    // InternalMyDsl.g:1887:1: rule__Site__WeatherfunctAssignment_10_1 : ( ruleWeatherFunct ) ;
    public final void rule__Site__WeatherfunctAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1891:1: ( ( ruleWeatherFunct ) )
            // InternalMyDsl.g:1892:2: ( ruleWeatherFunct )
            {
            // InternalMyDsl.g:1892:2: ( ruleWeatherFunct )
            // InternalMyDsl.g:1893:3: ruleWeatherFunct
            {
             before(grammarAccess.getSiteAccess().getWeatherfunctWeatherFunctParserRuleCall_10_1_0()); 
            pushFollow(FOLLOW_2);
            ruleWeatherFunct();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getWeatherfunctWeatherFunctParserRuleCall_10_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__WeatherfunctAssignment_10_1"


    // $ANTLR start "rule__Site__DisplayAssignment_11_1"
    // InternalMyDsl.g:1902:1: rule__Site__DisplayAssignment_11_1 : ( ruleItemNumberFunct ) ;
    public final void rule__Site__DisplayAssignment_11_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1906:1: ( ( ruleItemNumberFunct ) )
            // InternalMyDsl.g:1907:2: ( ruleItemNumberFunct )
            {
            // InternalMyDsl.g:1907:2: ( ruleItemNumberFunct )
            // InternalMyDsl.g:1908:3: ruleItemNumberFunct
            {
             before(grammarAccess.getSiteAccess().getDisplayItemNumberFunctParserRuleCall_11_1_0()); 
            pushFollow(FOLLOW_2);
            ruleItemNumberFunct();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getDisplayItemNumberFunctParserRuleCall_11_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__DisplayAssignment_11_1"


    // $ANTLR start "rule__Site__DisplayAssignment_11_2"
    // InternalMyDsl.g:1917:1: rule__Site__DisplayAssignment_11_2 : ( ruleColorPickerFunct ) ;
    public final void rule__Site__DisplayAssignment_11_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1921:1: ( ( ruleColorPickerFunct ) )
            // InternalMyDsl.g:1922:2: ( ruleColorPickerFunct )
            {
            // InternalMyDsl.g:1922:2: ( ruleColorPickerFunct )
            // InternalMyDsl.g:1923:3: ruleColorPickerFunct
            {
             before(grammarAccess.getSiteAccess().getDisplayColorPickerFunctParserRuleCall_11_2_0()); 
            pushFollow(FOLLOW_2);
            ruleColorPickerFunct();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getDisplayColorPickerFunctParserRuleCall_11_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__DisplayAssignment_11_2"


    // $ANTLR start "rule__Site__DisplayAssignment_11_3"
    // InternalMyDsl.g:1932:1: rule__Site__DisplayAssignment_11_3 : ( ruleDisplayItemFunct ) ;
    public final void rule__Site__DisplayAssignment_11_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1936:1: ( ( ruleDisplayItemFunct ) )
            // InternalMyDsl.g:1937:2: ( ruleDisplayItemFunct )
            {
            // InternalMyDsl.g:1937:2: ( ruleDisplayItemFunct )
            // InternalMyDsl.g:1938:3: ruleDisplayItemFunct
            {
             before(grammarAccess.getSiteAccess().getDisplayDisplayItemFunctParserRuleCall_11_3_0()); 
            pushFollow(FOLLOW_2);
            ruleDisplayItemFunct();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getDisplayDisplayItemFunctParserRuleCall_11_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__DisplayAssignment_11_3"


    // $ANTLR start "rule__Site__DisplayAssignment_11_4"
    // InternalMyDsl.g:1947:1: rule__Site__DisplayAssignment_11_4 : ( ruleSearchBarFunct ) ;
    public final void rule__Site__DisplayAssignment_11_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1951:1: ( ( ruleSearchBarFunct ) )
            // InternalMyDsl.g:1952:2: ( ruleSearchBarFunct )
            {
            // InternalMyDsl.g:1952:2: ( ruleSearchBarFunct )
            // InternalMyDsl.g:1953:3: ruleSearchBarFunct
            {
             before(grammarAccess.getSiteAccess().getDisplaySearchBarFunctParserRuleCall_11_4_0()); 
            pushFollow(FOLLOW_2);
            ruleSearchBarFunct();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getDisplaySearchBarFunctParserRuleCall_11_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__DisplayAssignment_11_4"


    // $ANTLR start "rule__Site__DisplayAssignment_11_5"
    // InternalMyDsl.g:1962:1: rule__Site__DisplayAssignment_11_5 : ( ruleSortBarFunct ) ;
    public final void rule__Site__DisplayAssignment_11_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1966:1: ( ( ruleSortBarFunct ) )
            // InternalMyDsl.g:1967:2: ( ruleSortBarFunct )
            {
            // InternalMyDsl.g:1967:2: ( ruleSortBarFunct )
            // InternalMyDsl.g:1968:3: ruleSortBarFunct
            {
             before(grammarAccess.getSiteAccess().getDisplaySortBarFunctParserRuleCall_11_5_0()); 
            pushFollow(FOLLOW_2);
            ruleSortBarFunct();

            state._fsp--;

             after(grammarAccess.getSiteAccess().getDisplaySortBarFunctParserRuleCall_11_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Site__DisplayAssignment_11_5"


    // $ANTLR start "rule__Company__NameAssignment_2"
    // InternalMyDsl.g:1977:1: rule__Company__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Company__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1981:1: ( ( ruleEString ) )
            // InternalMyDsl.g:1982:2: ( ruleEString )
            {
            // InternalMyDsl.g:1982:2: ( ruleEString )
            // InternalMyDsl.g:1983:3: ruleEString
            {
             before(grammarAccess.getCompanyAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCompanyAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Company__NameAssignment_2"


    // $ANTLR start "rule__Tag__NameAssignment_1"
    // InternalMyDsl.g:1992:1: rule__Tag__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Tag__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1996:1: ( ( ruleEString ) )
            // InternalMyDsl.g:1997:2: ( ruleEString )
            {
            // InternalMyDsl.g:1997:2: ( ruleEString )
            // InternalMyDsl.g:1998:3: ruleEString
            {
             before(grammarAccess.getTagAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTagAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__NameAssignment_1"


    // $ANTLR start "rule__Api__ApiNameAssignment_1"
    // InternalMyDsl.g:2007:1: rule__Api__ApiNameAssignment_1 : ( ruleAPIEnum ) ;
    public final void rule__Api__ApiNameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2011:1: ( ( ruleAPIEnum ) )
            // InternalMyDsl.g:2012:2: ( ruleAPIEnum )
            {
            // InternalMyDsl.g:2012:2: ( ruleAPIEnum )
            // InternalMyDsl.g:2013:3: ruleAPIEnum
            {
             before(grammarAccess.getApiAccess().getApiNameAPIEnumEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAPIEnum();

            state._fsp--;

             after(grammarAccess.getApiAccess().getApiNameAPIEnumEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Api__ApiNameAssignment_1"


    // $ANTLR start "rule__WeatherFunct__TownAssignment_1"
    // InternalMyDsl.g:2022:1: rule__WeatherFunct__TownAssignment_1 : ( ruleEString ) ;
    public final void rule__WeatherFunct__TownAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2026:1: ( ( ruleEString ) )
            // InternalMyDsl.g:2027:2: ( ruleEString )
            {
            // InternalMyDsl.g:2027:2: ( ruleEString )
            // InternalMyDsl.g:2028:3: ruleEString
            {
             before(grammarAccess.getWeatherFunctAccess().getTownEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getWeatherFunctAccess().getTownEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__TownAssignment_1"


    // $ANTLR start "rule__WeatherFunct__CountryAssignment_3"
    // InternalMyDsl.g:2037:1: rule__WeatherFunct__CountryAssignment_3 : ( ruleEString ) ;
    public final void rule__WeatherFunct__CountryAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2041:1: ( ( ruleEString ) )
            // InternalMyDsl.g:2042:2: ( ruleEString )
            {
            // InternalMyDsl.g:2042:2: ( ruleEString )
            // InternalMyDsl.g:2043:3: ruleEString
            {
             before(grammarAccess.getWeatherFunctAccess().getCountryEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getWeatherFunctAccess().getCountryEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WeatherFunct__CountryAssignment_3"


    // $ANTLR start "rule__ItemNumberFunct__NumberAssignment_3"
    // InternalMyDsl.g:2052:1: rule__ItemNumberFunct__NumberAssignment_3 : ( ruleEInt ) ;
    public final void rule__ItemNumberFunct__NumberAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2056:1: ( ( ruleEInt ) )
            // InternalMyDsl.g:2057:2: ( ruleEInt )
            {
            // InternalMyDsl.g:2057:2: ( ruleEInt )
            // InternalMyDsl.g:2058:3: ruleEInt
            {
             before(grammarAccess.getItemNumberFunctAccess().getNumberEIntParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getItemNumberFunctAccess().getNumberEIntParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ItemNumberFunct__NumberAssignment_3"


    // $ANTLR start "rule__ColorPickerFunct__DefaultColorAssignment_3"
    // InternalMyDsl.g:2067:1: rule__ColorPickerFunct__DefaultColorAssignment_3 : ( ruleEString ) ;
    public final void rule__ColorPickerFunct__DefaultColorAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2071:1: ( ( ruleEString ) )
            // InternalMyDsl.g:2072:2: ( ruleEString )
            {
            // InternalMyDsl.g:2072:2: ( ruleEString )
            // InternalMyDsl.g:2073:3: ruleEString
            {
             before(grammarAccess.getColorPickerFunctAccess().getDefaultColorEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getColorPickerFunctAccess().getDefaultColorEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorPickerFunct__DefaultColorAssignment_3"


    // $ANTLR start "rule__DisplayItemFunct__DefaultDisplayItemAssignment_2"
    // InternalMyDsl.g:2082:1: rule__DisplayItemFunct__DefaultDisplayItemAssignment_2 : ( ruleDisplayItemChoiceEnum ) ;
    public final void rule__DisplayItemFunct__DefaultDisplayItemAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2086:1: ( ( ruleDisplayItemChoiceEnum ) )
            // InternalMyDsl.g:2087:2: ( ruleDisplayItemChoiceEnum )
            {
            // InternalMyDsl.g:2087:2: ( ruleDisplayItemChoiceEnum )
            // InternalMyDsl.g:2088:3: ruleDisplayItemChoiceEnum
            {
             before(grammarAccess.getDisplayItemFunctAccess().getDefaultDisplayItemDisplayItemChoiceEnumEnumRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDisplayItemChoiceEnum();

            state._fsp--;

             after(grammarAccess.getDisplayItemFunctAccess().getDefaultDisplayItemDisplayItemChoiceEnumEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayItemFunct__DefaultDisplayItemAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000001C0000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000075000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000040000000L});

}