/**
 * generated by Xtext 2.14.0
 */
package com.colab.mydsl;

import com.colab.mydsl.AbstractMyDslRuntimeModule;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
@SuppressWarnings("all")
public class MyDslRuntimeModule extends AbstractMyDslRuntimeModule {
}
