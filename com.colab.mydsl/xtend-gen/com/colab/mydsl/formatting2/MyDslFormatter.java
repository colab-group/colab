/**
 * generated by Xtext 2.14.0
 */
package com.colab.mydsl.formatting2;

import colabDSL.Company;
import colabDSL.Site;
import colabDSL.Tag;
import com.colab.mydsl.services.MyDslGrammarAccess;
import com.google.inject.Inject;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.formatting2.AbstractFormatter2;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class MyDslFormatter extends AbstractFormatter2 {
  @Inject
  @Extension
  private MyDslGrammarAccess _myDslGrammarAccess;
  
  protected void _format(final Site site, @Extension final IFormattableDocument document) {
    document.<Company>format(site.getCompany());
    EList<Tag> _tag = site.getTag();
    for (final Tag tag : _tag) {
      document.<Tag>format(tag);
    }
  }
  
  public void format(final Object site, final IFormattableDocument document) {
    if (site instanceof XtextResource) {
      _format((XtextResource)site, document);
      return;
    } else if (site instanceof Site) {
      _format((Site)site, document);
      return;
    } else if (site instanceof EObject) {
      _format((EObject)site, document);
      return;
    } else if (site == null) {
      _format((Void)null, document);
      return;
    } else if (site != null) {
      _format(site, document);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(site, document).toString());
    }
  }
}
