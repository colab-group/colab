package com.colab.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.colab.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_STRING", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'site'", "'apis'", "','", "'tags'", "'weather'", "'settings :'", "'company'", "'town'", "'country'", "'itemVideo'", "'number'", "'colorPicker'", "'default'", "'displayItem'", "'searchBar'", "'sortBar'", "'dailymotion'", "'deezer'", "'LIST'", "'MOZAIC'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int RULE_ID=6;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Site";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSite"
    // InternalMyDsl.g:65:1: entryRuleSite returns [EObject current=null] : iv_ruleSite= ruleSite EOF ;
    public final EObject entryRuleSite() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSite = null;


        try {
            // InternalMyDsl.g:65:45: (iv_ruleSite= ruleSite EOF )
            // InternalMyDsl.g:66:2: iv_ruleSite= ruleSite EOF
            {
             newCompositeNode(grammarAccess.getSiteRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSite=ruleSite();

            state._fsp--;

             current =iv_ruleSite; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSite"


    // $ANTLR start "ruleSite"
    // InternalMyDsl.g:72:1: ruleSite returns [EObject current=null] : ( () otherlv_1= 'site' ( (lv_name_2_0= ruleEString ) ) ( (lv_company_3_0= ruleCompany ) ) otherlv_4= 'apis' ( (lv_api_5_0= ruleApi ) ) (otherlv_6= ',' ( (lv_api_7_0= ruleApi ) ) )* otherlv_8= 'tags' ( (lv_tag_9_0= ruleTag ) ) (otherlv_10= ',' ( (lv_tag_11_0= ruleTag ) ) )* (otherlv_12= 'weather' ( (lv_weatherfunct_13_0= ruleWeatherFunct ) ) )? (otherlv_14= 'settings :' ( (lv_display_15_0= ruleItemNumberFunct ) )? ( (lv_display_16_0= ruleColorPickerFunct ) )? ( (lv_display_17_0= ruleDisplayItemFunct ) )? ( (lv_display_18_0= ruleSearchBarFunct ) )? ( (lv_display_19_0= ruleSortBarFunct ) )? )? ) ;
    public final EObject ruleSite() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_company_3_0 = null;

        EObject lv_api_5_0 = null;

        EObject lv_api_7_0 = null;

        EObject lv_tag_9_0 = null;

        EObject lv_tag_11_0 = null;

        EObject lv_weatherfunct_13_0 = null;

        EObject lv_display_15_0 = null;

        EObject lv_display_16_0 = null;

        EObject lv_display_17_0 = null;

        EObject lv_display_18_0 = null;

        EObject lv_display_19_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:78:2: ( ( () otherlv_1= 'site' ( (lv_name_2_0= ruleEString ) ) ( (lv_company_3_0= ruleCompany ) ) otherlv_4= 'apis' ( (lv_api_5_0= ruleApi ) ) (otherlv_6= ',' ( (lv_api_7_0= ruleApi ) ) )* otherlv_8= 'tags' ( (lv_tag_9_0= ruleTag ) ) (otherlv_10= ',' ( (lv_tag_11_0= ruleTag ) ) )* (otherlv_12= 'weather' ( (lv_weatherfunct_13_0= ruleWeatherFunct ) ) )? (otherlv_14= 'settings :' ( (lv_display_15_0= ruleItemNumberFunct ) )? ( (lv_display_16_0= ruleColorPickerFunct ) )? ( (lv_display_17_0= ruleDisplayItemFunct ) )? ( (lv_display_18_0= ruleSearchBarFunct ) )? ( (lv_display_19_0= ruleSortBarFunct ) )? )? ) )
            // InternalMyDsl.g:79:2: ( () otherlv_1= 'site' ( (lv_name_2_0= ruleEString ) ) ( (lv_company_3_0= ruleCompany ) ) otherlv_4= 'apis' ( (lv_api_5_0= ruleApi ) ) (otherlv_6= ',' ( (lv_api_7_0= ruleApi ) ) )* otherlv_8= 'tags' ( (lv_tag_9_0= ruleTag ) ) (otherlv_10= ',' ( (lv_tag_11_0= ruleTag ) ) )* (otherlv_12= 'weather' ( (lv_weatherfunct_13_0= ruleWeatherFunct ) ) )? (otherlv_14= 'settings :' ( (lv_display_15_0= ruleItemNumberFunct ) )? ( (lv_display_16_0= ruleColorPickerFunct ) )? ( (lv_display_17_0= ruleDisplayItemFunct ) )? ( (lv_display_18_0= ruleSearchBarFunct ) )? ( (lv_display_19_0= ruleSortBarFunct ) )? )? )
            {
            // InternalMyDsl.g:79:2: ( () otherlv_1= 'site' ( (lv_name_2_0= ruleEString ) ) ( (lv_company_3_0= ruleCompany ) ) otherlv_4= 'apis' ( (lv_api_5_0= ruleApi ) ) (otherlv_6= ',' ( (lv_api_7_0= ruleApi ) ) )* otherlv_8= 'tags' ( (lv_tag_9_0= ruleTag ) ) (otherlv_10= ',' ( (lv_tag_11_0= ruleTag ) ) )* (otherlv_12= 'weather' ( (lv_weatherfunct_13_0= ruleWeatherFunct ) ) )? (otherlv_14= 'settings :' ( (lv_display_15_0= ruleItemNumberFunct ) )? ( (lv_display_16_0= ruleColorPickerFunct ) )? ( (lv_display_17_0= ruleDisplayItemFunct ) )? ( (lv_display_18_0= ruleSearchBarFunct ) )? ( (lv_display_19_0= ruleSortBarFunct ) )? )? )
            // InternalMyDsl.g:80:3: () otherlv_1= 'site' ( (lv_name_2_0= ruleEString ) ) ( (lv_company_3_0= ruleCompany ) ) otherlv_4= 'apis' ( (lv_api_5_0= ruleApi ) ) (otherlv_6= ',' ( (lv_api_7_0= ruleApi ) ) )* otherlv_8= 'tags' ( (lv_tag_9_0= ruleTag ) ) (otherlv_10= ',' ( (lv_tag_11_0= ruleTag ) ) )* (otherlv_12= 'weather' ( (lv_weatherfunct_13_0= ruleWeatherFunct ) ) )? (otherlv_14= 'settings :' ( (lv_display_15_0= ruleItemNumberFunct ) )? ( (lv_display_16_0= ruleColorPickerFunct ) )? ( (lv_display_17_0= ruleDisplayItemFunct ) )? ( (lv_display_18_0= ruleSearchBarFunct ) )? ( (lv_display_19_0= ruleSortBarFunct ) )? )?
            {
            // InternalMyDsl.g:80:3: ()
            // InternalMyDsl.g:81:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSiteAccess().getSiteAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getSiteAccess().getSiteKeyword_1());
            		
            // InternalMyDsl.g:91:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMyDsl.g:92:4: (lv_name_2_0= ruleEString )
            {
            // InternalMyDsl.g:92:4: (lv_name_2_0= ruleEString )
            // InternalMyDsl.g:93:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSiteAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSiteRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.colab.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMyDsl.g:110:3: ( (lv_company_3_0= ruleCompany ) )
            // InternalMyDsl.g:111:4: (lv_company_3_0= ruleCompany )
            {
            // InternalMyDsl.g:111:4: (lv_company_3_0= ruleCompany )
            // InternalMyDsl.g:112:5: lv_company_3_0= ruleCompany
            {

            					newCompositeNode(grammarAccess.getSiteAccess().getCompanyCompanyParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_5);
            lv_company_3_0=ruleCompany();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSiteRule());
            					}
            					set(
            						current,
            						"company",
            						lv_company_3_0,
            						"com.colab.mydsl.MyDsl.Company");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getSiteAccess().getApisKeyword_4());
            		
            // InternalMyDsl.g:133:3: ( (lv_api_5_0= ruleApi ) )
            // InternalMyDsl.g:134:4: (lv_api_5_0= ruleApi )
            {
            // InternalMyDsl.g:134:4: (lv_api_5_0= ruleApi )
            // InternalMyDsl.g:135:5: lv_api_5_0= ruleApi
            {

            					newCompositeNode(grammarAccess.getSiteAccess().getApiApiParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_7);
            lv_api_5_0=ruleApi();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSiteRule());
            					}
            					add(
            						current,
            						"api",
            						lv_api_5_0,
            						"com.colab.mydsl.MyDsl.Api");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMyDsl.g:152:3: (otherlv_6= ',' ( (lv_api_7_0= ruleApi ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==13) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:153:4: otherlv_6= ',' ( (lv_api_7_0= ruleApi ) )
            	    {
            	    otherlv_6=(Token)match(input,13,FOLLOW_6); 

            	    				newLeafNode(otherlv_6, grammarAccess.getSiteAccess().getCommaKeyword_6_0());
            	    			
            	    // InternalMyDsl.g:157:4: ( (lv_api_7_0= ruleApi ) )
            	    // InternalMyDsl.g:158:5: (lv_api_7_0= ruleApi )
            	    {
            	    // InternalMyDsl.g:158:5: (lv_api_7_0= ruleApi )
            	    // InternalMyDsl.g:159:6: lv_api_7_0= ruleApi
            	    {

            	    						newCompositeNode(grammarAccess.getSiteAccess().getApiApiParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_api_7_0=ruleApi();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getSiteRule());
            	    						}
            	    						add(
            	    							current,
            	    							"api",
            	    							lv_api_7_0,
            	    							"com.colab.mydsl.MyDsl.Api");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_8=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_8, grammarAccess.getSiteAccess().getTagsKeyword_7());
            		
            // InternalMyDsl.g:181:3: ( (lv_tag_9_0= ruleTag ) )
            // InternalMyDsl.g:182:4: (lv_tag_9_0= ruleTag )
            {
            // InternalMyDsl.g:182:4: (lv_tag_9_0= ruleTag )
            // InternalMyDsl.g:183:5: lv_tag_9_0= ruleTag
            {

            					newCompositeNode(grammarAccess.getSiteAccess().getTagTagParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_8);
            lv_tag_9_0=ruleTag();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSiteRule());
            					}
            					add(
            						current,
            						"tag",
            						lv_tag_9_0,
            						"com.colab.mydsl.MyDsl.Tag");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMyDsl.g:200:3: (otherlv_10= ',' ( (lv_tag_11_0= ruleTag ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalMyDsl.g:201:4: otherlv_10= ',' ( (lv_tag_11_0= ruleTag ) )
            	    {
            	    otherlv_10=(Token)match(input,13,FOLLOW_3); 

            	    				newLeafNode(otherlv_10, grammarAccess.getSiteAccess().getCommaKeyword_9_0());
            	    			
            	    // InternalMyDsl.g:205:4: ( (lv_tag_11_0= ruleTag ) )
            	    // InternalMyDsl.g:206:5: (lv_tag_11_0= ruleTag )
            	    {
            	    // InternalMyDsl.g:206:5: (lv_tag_11_0= ruleTag )
            	    // InternalMyDsl.g:207:6: lv_tag_11_0= ruleTag
            	    {

            	    						newCompositeNode(grammarAccess.getSiteAccess().getTagTagParserRuleCall_9_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_tag_11_0=ruleTag();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getSiteRule());
            	    						}
            	    						add(
            	    							current,
            	    							"tag",
            	    							lv_tag_11_0,
            	    							"com.colab.mydsl.MyDsl.Tag");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalMyDsl.g:225:3: (otherlv_12= 'weather' ( (lv_weatherfunct_13_0= ruleWeatherFunct ) ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:226:4: otherlv_12= 'weather' ( (lv_weatherfunct_13_0= ruleWeatherFunct ) )
                    {
                    otherlv_12=(Token)match(input,15,FOLLOW_9); 

                    				newLeafNode(otherlv_12, grammarAccess.getSiteAccess().getWeatherKeyword_10_0());
                    			
                    // InternalMyDsl.g:230:4: ( (lv_weatherfunct_13_0= ruleWeatherFunct ) )
                    // InternalMyDsl.g:231:5: (lv_weatherfunct_13_0= ruleWeatherFunct )
                    {
                    // InternalMyDsl.g:231:5: (lv_weatherfunct_13_0= ruleWeatherFunct )
                    // InternalMyDsl.g:232:6: lv_weatherfunct_13_0= ruleWeatherFunct
                    {

                    						newCompositeNode(grammarAccess.getSiteAccess().getWeatherfunctWeatherFunctParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_weatherfunct_13_0=ruleWeatherFunct();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSiteRule());
                    						}
                    						set(
                    							current,
                    							"weatherfunct",
                    							lv_weatherfunct_13_0,
                    							"com.colab.mydsl.MyDsl.WeatherFunct");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:250:3: (otherlv_14= 'settings :' ( (lv_display_15_0= ruleItemNumberFunct ) )? ( (lv_display_16_0= ruleColorPickerFunct ) )? ( (lv_display_17_0= ruleDisplayItemFunct ) )? ( (lv_display_18_0= ruleSearchBarFunct ) )? ( (lv_display_19_0= ruleSortBarFunct ) )? )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==16) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalMyDsl.g:251:4: otherlv_14= 'settings :' ( (lv_display_15_0= ruleItemNumberFunct ) )? ( (lv_display_16_0= ruleColorPickerFunct ) )? ( (lv_display_17_0= ruleDisplayItemFunct ) )? ( (lv_display_18_0= ruleSearchBarFunct ) )? ( (lv_display_19_0= ruleSortBarFunct ) )?
                    {
                    otherlv_14=(Token)match(input,16,FOLLOW_11); 

                    				newLeafNode(otherlv_14, grammarAccess.getSiteAccess().getSettingsKeyword_11_0());
                    			
                    // InternalMyDsl.g:255:4: ( (lv_display_15_0= ruleItemNumberFunct ) )?
                    int alt4=2;
                    int LA4_0 = input.LA(1);

                    if ( (LA4_0==20) ) {
                        alt4=1;
                    }
                    switch (alt4) {
                        case 1 :
                            // InternalMyDsl.g:256:5: (lv_display_15_0= ruleItemNumberFunct )
                            {
                            // InternalMyDsl.g:256:5: (lv_display_15_0= ruleItemNumberFunct )
                            // InternalMyDsl.g:257:6: lv_display_15_0= ruleItemNumberFunct
                            {

                            						newCompositeNode(grammarAccess.getSiteAccess().getDisplayItemNumberFunctParserRuleCall_11_1_0());
                            					
                            pushFollow(FOLLOW_12);
                            lv_display_15_0=ruleItemNumberFunct();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSiteRule());
                            						}
                            						add(
                            							current,
                            							"display",
                            							lv_display_15_0,
                            							"com.colab.mydsl.MyDsl.ItemNumberFunct");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalMyDsl.g:274:4: ( (lv_display_16_0= ruleColorPickerFunct ) )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==22) ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // InternalMyDsl.g:275:5: (lv_display_16_0= ruleColorPickerFunct )
                            {
                            // InternalMyDsl.g:275:5: (lv_display_16_0= ruleColorPickerFunct )
                            // InternalMyDsl.g:276:6: lv_display_16_0= ruleColorPickerFunct
                            {

                            						newCompositeNode(grammarAccess.getSiteAccess().getDisplayColorPickerFunctParserRuleCall_11_2_0());
                            					
                            pushFollow(FOLLOW_13);
                            lv_display_16_0=ruleColorPickerFunct();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSiteRule());
                            						}
                            						add(
                            							current,
                            							"display",
                            							lv_display_16_0,
                            							"com.colab.mydsl.MyDsl.ColorPickerFunct");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalMyDsl.g:293:4: ( (lv_display_17_0= ruleDisplayItemFunct ) )?
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==24) ) {
                        alt6=1;
                    }
                    switch (alt6) {
                        case 1 :
                            // InternalMyDsl.g:294:5: (lv_display_17_0= ruleDisplayItemFunct )
                            {
                            // InternalMyDsl.g:294:5: (lv_display_17_0= ruleDisplayItemFunct )
                            // InternalMyDsl.g:295:6: lv_display_17_0= ruleDisplayItemFunct
                            {

                            						newCompositeNode(grammarAccess.getSiteAccess().getDisplayDisplayItemFunctParserRuleCall_11_3_0());
                            					
                            pushFollow(FOLLOW_14);
                            lv_display_17_0=ruleDisplayItemFunct();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSiteRule());
                            						}
                            						add(
                            							current,
                            							"display",
                            							lv_display_17_0,
                            							"com.colab.mydsl.MyDsl.DisplayItemFunct");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalMyDsl.g:312:4: ( (lv_display_18_0= ruleSearchBarFunct ) )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==25) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalMyDsl.g:313:5: (lv_display_18_0= ruleSearchBarFunct )
                            {
                            // InternalMyDsl.g:313:5: (lv_display_18_0= ruleSearchBarFunct )
                            // InternalMyDsl.g:314:6: lv_display_18_0= ruleSearchBarFunct
                            {

                            						newCompositeNode(grammarAccess.getSiteAccess().getDisplaySearchBarFunctParserRuleCall_11_4_0());
                            					
                            pushFollow(FOLLOW_15);
                            lv_display_18_0=ruleSearchBarFunct();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSiteRule());
                            						}
                            						add(
                            							current,
                            							"display",
                            							lv_display_18_0,
                            							"com.colab.mydsl.MyDsl.SearchBarFunct");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalMyDsl.g:331:4: ( (lv_display_19_0= ruleSortBarFunct ) )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==26) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // InternalMyDsl.g:332:5: (lv_display_19_0= ruleSortBarFunct )
                            {
                            // InternalMyDsl.g:332:5: (lv_display_19_0= ruleSortBarFunct )
                            // InternalMyDsl.g:333:6: lv_display_19_0= ruleSortBarFunct
                            {

                            						newCompositeNode(grammarAccess.getSiteAccess().getDisplaySortBarFunctParserRuleCall_11_5_0());
                            					
                            pushFollow(FOLLOW_2);
                            lv_display_19_0=ruleSortBarFunct();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSiteRule());
                            						}
                            						add(
                            							current,
                            							"display",
                            							lv_display_19_0,
                            							"com.colab.mydsl.MyDsl.SortBarFunct");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSite"


    // $ANTLR start "entryRuleCompany"
    // InternalMyDsl.g:355:1: entryRuleCompany returns [EObject current=null] : iv_ruleCompany= ruleCompany EOF ;
    public final EObject entryRuleCompany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompany = null;


        try {
            // InternalMyDsl.g:355:48: (iv_ruleCompany= ruleCompany EOF )
            // InternalMyDsl.g:356:2: iv_ruleCompany= ruleCompany EOF
            {
             newCompositeNode(grammarAccess.getCompanyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCompany=ruleCompany();

            state._fsp--;

             current =iv_ruleCompany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompany"


    // $ANTLR start "ruleCompany"
    // InternalMyDsl.g:362:1: ruleCompany returns [EObject current=null] : ( () otherlv_1= 'company' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleCompany() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:368:2: ( ( () otherlv_1= 'company' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalMyDsl.g:369:2: ( () otherlv_1= 'company' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalMyDsl.g:369:2: ( () otherlv_1= 'company' ( (lv_name_2_0= ruleEString ) ) )
            // InternalMyDsl.g:370:3: () otherlv_1= 'company' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalMyDsl.g:370:3: ()
            // InternalMyDsl.g:371:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCompanyAccess().getCompanyAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getCompanyAccess().getCompanyKeyword_1());
            		
            // InternalMyDsl.g:381:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMyDsl.g:382:4: (lv_name_2_0= ruleEString )
            {
            // InternalMyDsl.g:382:4: (lv_name_2_0= ruleEString )
            // InternalMyDsl.g:383:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCompanyAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCompanyRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.colab.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompany"


    // $ANTLR start "entryRuleTag"
    // InternalMyDsl.g:404:1: entryRuleTag returns [EObject current=null] : iv_ruleTag= ruleTag EOF ;
    public final EObject entryRuleTag() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTag = null;


        try {
            // InternalMyDsl.g:404:44: (iv_ruleTag= ruleTag EOF )
            // InternalMyDsl.g:405:2: iv_ruleTag= ruleTag EOF
            {
             newCompositeNode(grammarAccess.getTagRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTag=ruleTag();

            state._fsp--;

             current =iv_ruleTag; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTag"


    // $ANTLR start "ruleTag"
    // InternalMyDsl.g:411:1: ruleTag returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleTag() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:417:2: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // InternalMyDsl.g:418:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // InternalMyDsl.g:418:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            // InternalMyDsl.g:419:3: () ( (lv_name_1_0= ruleEString ) )
            {
            // InternalMyDsl.g:419:3: ()
            // InternalMyDsl.g:420:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTagAccess().getTagAction_0(),
            					current);
            			

            }

            // InternalMyDsl.g:426:3: ( (lv_name_1_0= ruleEString ) )
            // InternalMyDsl.g:427:4: (lv_name_1_0= ruleEString )
            {
            // InternalMyDsl.g:427:4: (lv_name_1_0= ruleEString )
            // InternalMyDsl.g:428:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getTagAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTagRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"com.colab.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTag"


    // $ANTLR start "entryRuleApi"
    // InternalMyDsl.g:449:1: entryRuleApi returns [EObject current=null] : iv_ruleApi= ruleApi EOF ;
    public final EObject entryRuleApi() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleApi = null;


        try {
            // InternalMyDsl.g:449:44: (iv_ruleApi= ruleApi EOF )
            // InternalMyDsl.g:450:2: iv_ruleApi= ruleApi EOF
            {
             newCompositeNode(grammarAccess.getApiRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleApi=ruleApi();

            state._fsp--;

             current =iv_ruleApi; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleApi"


    // $ANTLR start "ruleApi"
    // InternalMyDsl.g:456:1: ruleApi returns [EObject current=null] : ( () ( (lv_apiName_1_0= ruleAPIEnum ) ) ) ;
    public final EObject ruleApi() throws RecognitionException {
        EObject current = null;

        Enumerator lv_apiName_1_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:462:2: ( ( () ( (lv_apiName_1_0= ruleAPIEnum ) ) ) )
            // InternalMyDsl.g:463:2: ( () ( (lv_apiName_1_0= ruleAPIEnum ) ) )
            {
            // InternalMyDsl.g:463:2: ( () ( (lv_apiName_1_0= ruleAPIEnum ) ) )
            // InternalMyDsl.g:464:3: () ( (lv_apiName_1_0= ruleAPIEnum ) )
            {
            // InternalMyDsl.g:464:3: ()
            // InternalMyDsl.g:465:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getApiAccess().getApiAction_0(),
            					current);
            			

            }

            // InternalMyDsl.g:471:3: ( (lv_apiName_1_0= ruleAPIEnum ) )
            // InternalMyDsl.g:472:4: (lv_apiName_1_0= ruleAPIEnum )
            {
            // InternalMyDsl.g:472:4: (lv_apiName_1_0= ruleAPIEnum )
            // InternalMyDsl.g:473:5: lv_apiName_1_0= ruleAPIEnum
            {

            					newCompositeNode(grammarAccess.getApiAccess().getApiNameAPIEnumEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_apiName_1_0=ruleAPIEnum();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getApiRule());
            					}
            					set(
            						current,
            						"apiName",
            						lv_apiName_1_0,
            						"com.colab.mydsl.MyDsl.APIEnum");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleApi"


    // $ANTLR start "entryRuleWeatherFunct"
    // InternalMyDsl.g:494:1: entryRuleWeatherFunct returns [EObject current=null] : iv_ruleWeatherFunct= ruleWeatherFunct EOF ;
    public final EObject entryRuleWeatherFunct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWeatherFunct = null;


        try {
            // InternalMyDsl.g:494:53: (iv_ruleWeatherFunct= ruleWeatherFunct EOF )
            // InternalMyDsl.g:495:2: iv_ruleWeatherFunct= ruleWeatherFunct EOF
            {
             newCompositeNode(grammarAccess.getWeatherFunctRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWeatherFunct=ruleWeatherFunct();

            state._fsp--;

             current =iv_ruleWeatherFunct; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWeatherFunct"


    // $ANTLR start "ruleWeatherFunct"
    // InternalMyDsl.g:501:1: ruleWeatherFunct returns [EObject current=null] : (otherlv_0= 'town' ( (lv_town_1_0= ruleEString ) ) otherlv_2= 'country' ( (lv_country_3_0= ruleEString ) ) ) ;
    public final EObject ruleWeatherFunct() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_town_1_0 = null;

        AntlrDatatypeRuleToken lv_country_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:507:2: ( (otherlv_0= 'town' ( (lv_town_1_0= ruleEString ) ) otherlv_2= 'country' ( (lv_country_3_0= ruleEString ) ) ) )
            // InternalMyDsl.g:508:2: (otherlv_0= 'town' ( (lv_town_1_0= ruleEString ) ) otherlv_2= 'country' ( (lv_country_3_0= ruleEString ) ) )
            {
            // InternalMyDsl.g:508:2: (otherlv_0= 'town' ( (lv_town_1_0= ruleEString ) ) otherlv_2= 'country' ( (lv_country_3_0= ruleEString ) ) )
            // InternalMyDsl.g:509:3: otherlv_0= 'town' ( (lv_town_1_0= ruleEString ) ) otherlv_2= 'country' ( (lv_country_3_0= ruleEString ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getWeatherFunctAccess().getTownKeyword_0());
            		
            // InternalMyDsl.g:513:3: ( (lv_town_1_0= ruleEString ) )
            // InternalMyDsl.g:514:4: (lv_town_1_0= ruleEString )
            {
            // InternalMyDsl.g:514:4: (lv_town_1_0= ruleEString )
            // InternalMyDsl.g:515:5: lv_town_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWeatherFunctAccess().getTownEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_16);
            lv_town_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWeatherFunctRule());
            					}
            					set(
            						current,
            						"town",
            						lv_town_1_0,
            						"com.colab.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getWeatherFunctAccess().getCountryKeyword_2());
            		
            // InternalMyDsl.g:536:3: ( (lv_country_3_0= ruleEString ) )
            // InternalMyDsl.g:537:4: (lv_country_3_0= ruleEString )
            {
            // InternalMyDsl.g:537:4: (lv_country_3_0= ruleEString )
            // InternalMyDsl.g:538:5: lv_country_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWeatherFunctAccess().getCountryEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_country_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWeatherFunctRule());
            					}
            					set(
            						current,
            						"country",
            						lv_country_3_0,
            						"com.colab.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWeatherFunct"


    // $ANTLR start "entryRuleItemNumberFunct"
    // InternalMyDsl.g:559:1: entryRuleItemNumberFunct returns [EObject current=null] : iv_ruleItemNumberFunct= ruleItemNumberFunct EOF ;
    public final EObject entryRuleItemNumberFunct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleItemNumberFunct = null;


        try {
            // InternalMyDsl.g:559:56: (iv_ruleItemNumberFunct= ruleItemNumberFunct EOF )
            // InternalMyDsl.g:560:2: iv_ruleItemNumberFunct= ruleItemNumberFunct EOF
            {
             newCompositeNode(grammarAccess.getItemNumberFunctRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleItemNumberFunct=ruleItemNumberFunct();

            state._fsp--;

             current =iv_ruleItemNumberFunct; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleItemNumberFunct"


    // $ANTLR start "ruleItemNumberFunct"
    // InternalMyDsl.g:566:1: ruleItemNumberFunct returns [EObject current=null] : ( () otherlv_1= 'itemVideo' otherlv_2= 'number' ( (lv_number_3_0= ruleEInt ) ) ) ;
    public final EObject ruleItemNumberFunct() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_number_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:572:2: ( ( () otherlv_1= 'itemVideo' otherlv_2= 'number' ( (lv_number_3_0= ruleEInt ) ) ) )
            // InternalMyDsl.g:573:2: ( () otherlv_1= 'itemVideo' otherlv_2= 'number' ( (lv_number_3_0= ruleEInt ) ) )
            {
            // InternalMyDsl.g:573:2: ( () otherlv_1= 'itemVideo' otherlv_2= 'number' ( (lv_number_3_0= ruleEInt ) ) )
            // InternalMyDsl.g:574:3: () otherlv_1= 'itemVideo' otherlv_2= 'number' ( (lv_number_3_0= ruleEInt ) )
            {
            // InternalMyDsl.g:574:3: ()
            // InternalMyDsl.g:575:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getItemNumberFunctAccess().getItemNumberFunctAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,20,FOLLOW_17); 

            			newLeafNode(otherlv_1, grammarAccess.getItemNumberFunctAccess().getItemVideoKeyword_1());
            		
            otherlv_2=(Token)match(input,21,FOLLOW_18); 

            			newLeafNode(otherlv_2, grammarAccess.getItemNumberFunctAccess().getNumberKeyword_2());
            		
            // InternalMyDsl.g:589:3: ( (lv_number_3_0= ruleEInt ) )
            // InternalMyDsl.g:590:4: (lv_number_3_0= ruleEInt )
            {
            // InternalMyDsl.g:590:4: (lv_number_3_0= ruleEInt )
            // InternalMyDsl.g:591:5: lv_number_3_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getItemNumberFunctAccess().getNumberEIntParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_number_3_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getItemNumberFunctRule());
            					}
            					set(
            						current,
            						"number",
            						lv_number_3_0,
            						"com.colab.mydsl.MyDsl.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleItemNumberFunct"


    // $ANTLR start "entryRuleColorPickerFunct"
    // InternalMyDsl.g:612:1: entryRuleColorPickerFunct returns [EObject current=null] : iv_ruleColorPickerFunct= ruleColorPickerFunct EOF ;
    public final EObject entryRuleColorPickerFunct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleColorPickerFunct = null;


        try {
            // InternalMyDsl.g:612:57: (iv_ruleColorPickerFunct= ruleColorPickerFunct EOF )
            // InternalMyDsl.g:613:2: iv_ruleColorPickerFunct= ruleColorPickerFunct EOF
            {
             newCompositeNode(grammarAccess.getColorPickerFunctRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleColorPickerFunct=ruleColorPickerFunct();

            state._fsp--;

             current =iv_ruleColorPickerFunct; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleColorPickerFunct"


    // $ANTLR start "ruleColorPickerFunct"
    // InternalMyDsl.g:619:1: ruleColorPickerFunct returns [EObject current=null] : ( () otherlv_1= 'colorPicker' otherlv_2= 'default' ( (lv_defaultColor_3_0= ruleEString ) ) ) ;
    public final EObject ruleColorPickerFunct() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_defaultColor_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:625:2: ( ( () otherlv_1= 'colorPicker' otherlv_2= 'default' ( (lv_defaultColor_3_0= ruleEString ) ) ) )
            // InternalMyDsl.g:626:2: ( () otherlv_1= 'colorPicker' otherlv_2= 'default' ( (lv_defaultColor_3_0= ruleEString ) ) )
            {
            // InternalMyDsl.g:626:2: ( () otherlv_1= 'colorPicker' otherlv_2= 'default' ( (lv_defaultColor_3_0= ruleEString ) ) )
            // InternalMyDsl.g:627:3: () otherlv_1= 'colorPicker' otherlv_2= 'default' ( (lv_defaultColor_3_0= ruleEString ) )
            {
            // InternalMyDsl.g:627:3: ()
            // InternalMyDsl.g:628:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getColorPickerFunctAccess().getColorPickerFunctAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,22,FOLLOW_19); 

            			newLeafNode(otherlv_1, grammarAccess.getColorPickerFunctAccess().getColorPickerKeyword_1());
            		
            otherlv_2=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getColorPickerFunctAccess().getDefaultKeyword_2());
            		
            // InternalMyDsl.g:642:3: ( (lv_defaultColor_3_0= ruleEString ) )
            // InternalMyDsl.g:643:4: (lv_defaultColor_3_0= ruleEString )
            {
            // InternalMyDsl.g:643:4: (lv_defaultColor_3_0= ruleEString )
            // InternalMyDsl.g:644:5: lv_defaultColor_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getColorPickerFunctAccess().getDefaultColorEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_defaultColor_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getColorPickerFunctRule());
            					}
            					set(
            						current,
            						"defaultColor",
            						lv_defaultColor_3_0,
            						"com.colab.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleColorPickerFunct"


    // $ANTLR start "entryRuleDisplayItemFunct"
    // InternalMyDsl.g:665:1: entryRuleDisplayItemFunct returns [EObject current=null] : iv_ruleDisplayItemFunct= ruleDisplayItemFunct EOF ;
    public final EObject entryRuleDisplayItemFunct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisplayItemFunct = null;


        try {
            // InternalMyDsl.g:665:57: (iv_ruleDisplayItemFunct= ruleDisplayItemFunct EOF )
            // InternalMyDsl.g:666:2: iv_ruleDisplayItemFunct= ruleDisplayItemFunct EOF
            {
             newCompositeNode(grammarAccess.getDisplayItemFunctRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDisplayItemFunct=ruleDisplayItemFunct();

            state._fsp--;

             current =iv_ruleDisplayItemFunct; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisplayItemFunct"


    // $ANTLR start "ruleDisplayItemFunct"
    // InternalMyDsl.g:672:1: ruleDisplayItemFunct returns [EObject current=null] : (otherlv_0= 'displayItem' otherlv_1= 'default' ( (lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum ) ) ) ;
    public final EObject ruleDisplayItemFunct() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Enumerator lv_defaultDisplayItem_2_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:678:2: ( (otherlv_0= 'displayItem' otherlv_1= 'default' ( (lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum ) ) ) )
            // InternalMyDsl.g:679:2: (otherlv_0= 'displayItem' otherlv_1= 'default' ( (lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum ) ) )
            {
            // InternalMyDsl.g:679:2: (otherlv_0= 'displayItem' otherlv_1= 'default' ( (lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum ) ) )
            // InternalMyDsl.g:680:3: otherlv_0= 'displayItem' otherlv_1= 'default' ( (lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum ) )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getDisplayItemFunctAccess().getDisplayItemKeyword_0());
            		
            otherlv_1=(Token)match(input,23,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getDisplayItemFunctAccess().getDefaultKeyword_1());
            		
            // InternalMyDsl.g:688:3: ( (lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum ) )
            // InternalMyDsl.g:689:4: (lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum )
            {
            // InternalMyDsl.g:689:4: (lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum )
            // InternalMyDsl.g:690:5: lv_defaultDisplayItem_2_0= ruleDisplayItemChoiceEnum
            {

            					newCompositeNode(grammarAccess.getDisplayItemFunctAccess().getDefaultDisplayItemDisplayItemChoiceEnumEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_defaultDisplayItem_2_0=ruleDisplayItemChoiceEnum();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDisplayItemFunctRule());
            					}
            					set(
            						current,
            						"defaultDisplayItem",
            						lv_defaultDisplayItem_2_0,
            						"com.colab.mydsl.MyDsl.DisplayItemChoiceEnum");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisplayItemFunct"


    // $ANTLR start "entryRuleSearchBarFunct"
    // InternalMyDsl.g:711:1: entryRuleSearchBarFunct returns [EObject current=null] : iv_ruleSearchBarFunct= ruleSearchBarFunct EOF ;
    public final EObject entryRuleSearchBarFunct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSearchBarFunct = null;


        try {
            // InternalMyDsl.g:711:55: (iv_ruleSearchBarFunct= ruleSearchBarFunct EOF )
            // InternalMyDsl.g:712:2: iv_ruleSearchBarFunct= ruleSearchBarFunct EOF
            {
             newCompositeNode(grammarAccess.getSearchBarFunctRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSearchBarFunct=ruleSearchBarFunct();

            state._fsp--;

             current =iv_ruleSearchBarFunct; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSearchBarFunct"


    // $ANTLR start "ruleSearchBarFunct"
    // InternalMyDsl.g:718:1: ruleSearchBarFunct returns [EObject current=null] : ( () otherlv_1= 'searchBar' ) ;
    public final EObject ruleSearchBarFunct() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:724:2: ( ( () otherlv_1= 'searchBar' ) )
            // InternalMyDsl.g:725:2: ( () otherlv_1= 'searchBar' )
            {
            // InternalMyDsl.g:725:2: ( () otherlv_1= 'searchBar' )
            // InternalMyDsl.g:726:3: () otherlv_1= 'searchBar'
            {
            // InternalMyDsl.g:726:3: ()
            // InternalMyDsl.g:727:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSearchBarFunctAccess().getSearchBarFunctAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,25,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getSearchBarFunctAccess().getSearchBarKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSearchBarFunct"


    // $ANTLR start "entryRuleSortBarFunct"
    // InternalMyDsl.g:741:1: entryRuleSortBarFunct returns [EObject current=null] : iv_ruleSortBarFunct= ruleSortBarFunct EOF ;
    public final EObject entryRuleSortBarFunct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSortBarFunct = null;


        try {
            // InternalMyDsl.g:741:53: (iv_ruleSortBarFunct= ruleSortBarFunct EOF )
            // InternalMyDsl.g:742:2: iv_ruleSortBarFunct= ruleSortBarFunct EOF
            {
             newCompositeNode(grammarAccess.getSortBarFunctRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSortBarFunct=ruleSortBarFunct();

            state._fsp--;

             current =iv_ruleSortBarFunct; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSortBarFunct"


    // $ANTLR start "ruleSortBarFunct"
    // InternalMyDsl.g:748:1: ruleSortBarFunct returns [EObject current=null] : ( () otherlv_1= 'sortBar' ) ;
    public final EObject ruleSortBarFunct() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:754:2: ( ( () otherlv_1= 'sortBar' ) )
            // InternalMyDsl.g:755:2: ( () otherlv_1= 'sortBar' )
            {
            // InternalMyDsl.g:755:2: ( () otherlv_1= 'sortBar' )
            // InternalMyDsl.g:756:3: () otherlv_1= 'sortBar'
            {
            // InternalMyDsl.g:756:3: ()
            // InternalMyDsl.g:757:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSortBarFunctAccess().getSortBarFunctAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,26,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getSortBarFunctAccess().getSortBarKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSortBarFunct"


    // $ANTLR start "entryRuleEInt"
    // InternalMyDsl.g:771:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalMyDsl.g:771:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalMyDsl.g:772:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalMyDsl.g:778:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;


        	enterRule();

        try {
            // InternalMyDsl.g:784:2: (this_INT_0= RULE_INT )
            // InternalMyDsl.g:785:2: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            		current.merge(this_INT_0);
            	

            		newLeafNode(this_INT_0, grammarAccess.getEIntAccess().getINTTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleEString"
    // InternalMyDsl.g:795:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalMyDsl.g:795:47: (iv_ruleEString= ruleEString EOF )
            // InternalMyDsl.g:796:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalMyDsl.g:802:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:808:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalMyDsl.g:809:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalMyDsl.g:809:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_STRING) ) {
                alt10=1;
            }
            else if ( (LA10_0==RULE_ID) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalMyDsl.g:810:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:818:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "ruleAPIEnum"
    // InternalMyDsl.g:829:1: ruleAPIEnum returns [Enumerator current=null] : ( (enumLiteral_0= 'dailymotion' ) | (enumLiteral_1= 'deezer' ) ) ;
    public final Enumerator ruleAPIEnum() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:835:2: ( ( (enumLiteral_0= 'dailymotion' ) | (enumLiteral_1= 'deezer' ) ) )
            // InternalMyDsl.g:836:2: ( (enumLiteral_0= 'dailymotion' ) | (enumLiteral_1= 'deezer' ) )
            {
            // InternalMyDsl.g:836:2: ( (enumLiteral_0= 'dailymotion' ) | (enumLiteral_1= 'deezer' ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==27) ) {
                alt11=1;
            }
            else if ( (LA11_0==28) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalMyDsl.g:837:3: (enumLiteral_0= 'dailymotion' )
                    {
                    // InternalMyDsl.g:837:3: (enumLiteral_0= 'dailymotion' )
                    // InternalMyDsl.g:838:4: enumLiteral_0= 'dailymotion'
                    {
                    enumLiteral_0=(Token)match(input,27,FOLLOW_2); 

                    				current = grammarAccess.getAPIEnumAccess().getDailymotionEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getAPIEnumAccess().getDailymotionEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:845:3: (enumLiteral_1= 'deezer' )
                    {
                    // InternalMyDsl.g:845:3: (enumLiteral_1= 'deezer' )
                    // InternalMyDsl.g:846:4: enumLiteral_1= 'deezer'
                    {
                    enumLiteral_1=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getAPIEnumAccess().getDeezerEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getAPIEnumAccess().getDeezerEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAPIEnum"


    // $ANTLR start "ruleDisplayItemChoiceEnum"
    // InternalMyDsl.g:856:1: ruleDisplayItemChoiceEnum returns [Enumerator current=null] : ( (enumLiteral_0= 'LIST' ) | (enumLiteral_1= 'MOZAIC' ) ) ;
    public final Enumerator ruleDisplayItemChoiceEnum() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:862:2: ( ( (enumLiteral_0= 'LIST' ) | (enumLiteral_1= 'MOZAIC' ) ) )
            // InternalMyDsl.g:863:2: ( (enumLiteral_0= 'LIST' ) | (enumLiteral_1= 'MOZAIC' ) )
            {
            // InternalMyDsl.g:863:2: ( (enumLiteral_0= 'LIST' ) | (enumLiteral_1= 'MOZAIC' ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==29) ) {
                alt12=1;
            }
            else if ( (LA12_0==30) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalMyDsl.g:864:3: (enumLiteral_0= 'LIST' )
                    {
                    // InternalMyDsl.g:864:3: (enumLiteral_0= 'LIST' )
                    // InternalMyDsl.g:865:4: enumLiteral_0= 'LIST'
                    {
                    enumLiteral_0=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getDisplayItemChoiceEnumAccess().getLISTEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDisplayItemChoiceEnumAccess().getLISTEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:872:3: (enumLiteral_1= 'MOZAIC' )
                    {
                    // InternalMyDsl.g:872:3: (enumLiteral_1= 'MOZAIC' )
                    // InternalMyDsl.g:873:4: enumLiteral_1= 'MOZAIC'
                    {
                    enumLiteral_1=(Token)match(input,30,FOLLOW_2); 

                    				current = grammarAccess.getDisplayItemChoiceEnumAccess().getMOZAICEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDisplayItemChoiceEnumAccess().getMOZAICEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisplayItemChoiceEnum"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000001A002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000007500002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000007400002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000007000002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000006000002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000060000000L});

}