/*
 * generated by Xtext 2.14.0
 */
package com.colab.mydsl.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import colabDSL.Site
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import java.io.File
import org.eclipse.jgit.api.Git
import java.nio.file.Files
import java.nio.file.Paths
import colabDSL.Api
import colabDSL.Tag
import colabDSL.DisplayFunctionnality
import colabDSL.DisplayItemFunct
import colabDSL.ItemNumberFunct
import colabDSL.ColorPickerFunct
import colabDSL.SearchBarFunct
import colabDSL.SortBarFunct
import org.eclipse.xtend2.lib.StringConcatenation
import colabDSL.WeatherFunct

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class MyDslGenerator extends AbstractGenerator {
	
	private static final String FRONT_DATA_PATH = "colab-frontend/src/app/infos.ts";
	private static final String BACK_DATA_PATH = "backend-colab/src/main/resources/application.properties";

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		var site = resource.contents.get(0) as Site
		copyDepot("/tmp/colab-frontend", 'colab-frontend', "https://gitlab.com/colab-group/colab-frontend.git", fsa)
		copyDepot("/tmp/backend-colab", 'backend-colab', "https://gitlab.com/colab-group/backend-colab.git", fsa)
		generateNewFiles(fsa, site)
		buildGitLabCI(fsa)
	}
	
	def void generateNewFiles(IFileSystemAccess2 fsa, Site site) {
		
		var outFile = '''
			export const data = {
			    title: '«site.name»',
			    company: '«site.company.name»',
		'''
		
		var display = site.display.findFirst[i|i instanceof DisplayItemFunct] as DisplayItemFunct
		if (display != null) {
			outFile += '''
				displayEnable: true,
		        displayDefault: '«display.defaultDisplayItem»',
			'''
		} else {
			outFile += '''
				displayEnable: false,
		        displayDefault: 'LIST',
			'''
		}
		
		var itemNumber = site.display.findFirst[i|i instanceof ItemNumberFunct] as ItemNumberFunct
		if (itemNumber != null) {
			outFile += '''
				nbVideos: «itemNumber.number»,
			'''// nbVideos: 20,
		}
		
		var colorPicker = site.display.findFirst[i|i instanceof ColorPickerFunct] as ColorPickerFunct
		if (colorPicker != null) {
			outFile += '''
				color: '«colorPicker.defaultColor»',
		        colorPicker: true,
			'''
		} else {
			outFile += '''
				color: '#eeeeee',
		        colorPicker: false,
			'''
		}
		
		var searchBar = site.display.findFirst[i|i instanceof SearchBarFunct] as SearchBarFunct
		if (searchBar != null) {
			outFile += '''
				searchbar: true,
			'''
		} else {
			outFile += '''
				searchbar: false,
			'''
		}
		
		var sortBar = site.display.findFirst[i|i instanceof SortBarFunct] as SortBarFunct
		if (sortBar != null) {
			outFile += '''
				sortresult: true,
			'''
		} else {
			outFile += '''
				sortresult: false,
			'''
		}
		outFile += '''
				apis: [ 
			'''
		for(Api a: site.api) {
			outFile+= '''
			  '«a.apiName.toString.toUpperCase»',
			  '''
		}
		outFile = outFile.substring(0, outFile.length - 2)
		outFile += '''
				],
			'''
		outFile += '''
		};
		'''
		
		fsa.generateFile(MyDslGenerator.FRONT_DATA_PATH, outFile)

		var backData = '''properties.apis: ''';
		for(Api a: site.api) {
			backData+= '''«a.apiName»,'''
		}
		backData = backData.substring(0, backData.length - 1)
		backData += '''
			  
		properties.tags: ''';
		for(Tag t: site.tag) {
			backData+= '''«t.name»,'''
		}
		backData = backData.substring(0, backData.length - 1)
		if (site.weatherfunct != null) {
			backData += '''
				  
			properties.tagsbyweather=true
			properties.town: «site.weatherfunct.town»
			properties.country: «site.weatherfunct.country»''';
			
		} else {
			backData += '''
				  
			properties.tagsbyweather=false
			properties.town: Toulouse
			properties.country: fr''';
			
		}
		
		fsa.generateFile(MyDslGenerator.BACK_DATA_PATH, backData);
	}
	
	def void copyDepot(String tmpPath, String outputPath, String url, IFileSystemAccess2 fsa) {
		var gitFile = new File(tmpPath)
		if (!gitFile.isDirectory || gitFile.list.size == 0) {
			gitFile.mkdir
			var agit = Git.cloneRepository()
			  .setURI( url )
			  .setDirectory(gitFile)
			  .call();
//			  git.close
		}
		copyDirectory(outputPath, gitFile, fsa)
	}
	
	def void copyFile(String directoryOutput, String fileName, String path, IFileSystemAccess2 fsa) {
		fsa.generateFile(directoryOutput + '/' + fileName, new String(Files.readAllBytes(Paths.get(path))))
	}
	
	def void copyDirectory(String path, File file, IFileSystemAccess2 fsa) {
		var files = file.listFiles;
		files.forEach[f | 
			if (f.isDirectory) {
				if (!f.name.equals('.git')) {
					copyDirectory(path + '/' + f.name, f, fsa)
				}
				// Patch .mvn
				if (f.name.equals('.mvn')) {
					copyDirectory('../'+f.name, f, fsa)
				}
			} else {
				// Patch mvn wrapper
				if (f.name.equals('mvnw') || f.name.equals('mvnw.cmd')) {
					copyFile('../', f.name, f.path, fsa);
				} else {
					copyFile(path, f.name, f.path, fsa);
				}
			}
		]
	}
	
	def void buildGitLabCI(IFileSystemAccess2 fsa) {
		var outputFile = '''
		stages:
		  - build-backend
		  - deploy-backend
		  - build-frontend
		  - deploy-frontend
		
		
		before_script:
		 - echo "Execute scripts which are required to bootstrap the application. !"
		
		after_script:
		 - echo "Clean up activity can be done here !."
		
		
		npm-build:
		  image: node:latest
		  stage: build-frontend
		  script:
		    - cd src-gen/colab-frontend/
		    - npm run-script preinstall
		    - npm install
		    - npm run build --prod
		  tags:
		    - docker
		  artifacts:
		    paths:
		     - src-gen/colab-frontend/dist/
		     
		maven-build:
		  image: maven:3-jdk-8
		  stage: build-backend
		  script: 
		    - cd src-gen/backend-colab/
		    - mvn package -B
		  tags: 
		    - docker
		  artifacts:
		    paths:
		      - src-gen/backend-colab/target/*.jar
		
		
		deploy-front:
		  stage: deploy-frontend
		  image: ruby:2.3
		  when: on_success
		  dependencies:
		    - npm-build
		  tags:
		    - docker
		  script:
		    - cd src-gen/colab-frontend/
		    - apt-get update -qy
		    - apt-get install -y ruby-dev
		    - gem install dpl
		    - dpl --provider=heroku --app=colab-frontend --api-key=$HEROKU_API_KEY
		  only:
		    - master
		  environment:
		    name: prod-frontend
		    url: https://colab-frontend.herokuapp.com
		    
		deploy-back:
		  image: ruby:2.3
		  stage: deploy-backend
		  when: on_success
		  dependencies:
		    - maven-build
		  tags:
		    - docker
		  script:
		    - cd src-gen/backend-colab/
		    - apt-get update -qy
		    - apt-get install -y ruby-dev
		    - gem install dpl
		    - dpl --provider=heroku --app=colab-backend --api-key=$HEROKU_API_KEY
		  only:
		    - master
		  environment:
		    name: prod-backend
		    url: https://colab-backend.herokuapp.com
		'''
		fsa.generateFile('../.gitlab-ci.yml', outputFile)
		fsa.generateFile('.gitlab-ci.yml', outputFile)
	}
}
